# kusozako-music

a music player for linux

![home](screenshot/screenshot1.png)

![grid-view](screenshot/screenshot2.png)

![list-view](screenshot/screenshot3.png)

# License

This software is licensed undeer GPLv3 or any later version.

See LICENSE.md for more detail.

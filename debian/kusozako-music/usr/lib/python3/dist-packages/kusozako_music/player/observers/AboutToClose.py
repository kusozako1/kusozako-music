# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaAboutToClose(DeltaEntity):

    def _signal_received(self):
        playbin = self._enquiry("delta > playbin")
        playbin.set_state(Gst.State.NULL)
        playbin.set_state(Gst.State.PAUSED)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.ABOUT_TO_CLOSE:
            return
        self._signal_received()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)

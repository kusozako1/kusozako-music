# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music.const import ApplicationSignals
from kusozako_music.ApplicationObserver import AlfaApplicationObserver


class DeltaTogglePlayback(AlfaApplicationObserver):

    SIGNAL = ApplicationSignals.TOGGLE_PlAYBACK

    def _on_transmission_received(self, param=None):
        playbin = self._enquiry("delta > playbin")
        _, state_hook, _ = playbin.get_state(Gst.CLOCK_TIME_NONE)
        if state_hook == Gst.State.PLAYING:
            new_state = Gst.State.PAUSED
        else:
            new_state = Gst.State.PLAYING
        playbin.set_state(new_state)

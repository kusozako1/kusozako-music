# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaBus(DeltaEntity):

    def _timeout(self):
        data = ApplicationSignals.QUEUE_NEXT, None
        self._raise("delta > application signal", data)
        return GLib.SOURCE_REMOVE

    def _on_eos(self, *args):
        GLib.timeout_add_seconds(1, self._timeout)

    def _on_state_changed(self, gst_bus, gst_message):
        _, new_state, _ = gst_message.parse_state_changed()
        data = ApplicationSignals.PLAYBACK_STATE_CHANGED, new_state
        self._raise("delta > application signal", data)

    def __init__(self, parent):
        self._parent = parent
        playbin = self._enquiry("delta > playbin")
        bus = playbin.get_bus()
        bus.add_signal_watch()
        bus.connect("message::eos", self._on_eos)
        bus.connect('message::state-changed', self._on_state_changed)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ApplicationSignals
from kusozako_music.alfa.ApplicationObserver import AlfaApplicationObserver


class DeltaModelLayerReady(AlfaApplicationObserver):

    __signal__ = ApplicationSignals.MODEL_LAYER_READY

    def _signal_received(self, param=None):
        user_data = ApplicationSignals.QUEUE_NEXT, None
        self._raise("delta > application signal", user_data)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .observers.Observers import EchoObservers

RUN_FIRST = GObject.SIGNAL_RUN_FIRST
METADATA_CHANGED = (GObject.TYPE_PYOBJECT, GObject.TYPE_PYOBJECT)
NOW_PLAYING_CHANGED = (GObject.TYPE_PYOBJECT, )


class DeltaNowPlaying(GObject.Object, DeltaEntity):

    __gsignals__ = {
        "metadata-changed": (RUN_FIRST, None, METADATA_CHANGED),
        "now-playing-changed": (RUN_FIRST, None, NOW_PLAYING_CHANGED)
        }

    def _delta_info_current_index(self):
        return self.get_current_index()

    def _delta_call_new_file_info(self, file_info):
        self._file_info = file_info
        self.emit("now-playing-changed", self._file_info)
        title = file_info.get_attribute_string("kusozako1::title")
        user_data = MainWindowSignals.CHANGE_TITLE, title
        self._raise("delta > main window signal", user_data)

    def _delta_call_metadata_changed(self, user_data):
        file_info, buffer_ = user_data
        self.emit("metadata-changed", file_info, buffer_)

    def get_current_file_info(self):
        return self._file_info

    def get_current_index(self):
        model = self._enquiry("delta > viewer model")
        for index in range(0, model.get_n_items()):
            if model[index] == self._file_info:
                return index
        return -1

    def is_selected(self, index):
        model = self._enquiry("delta > viewer model")
        return model[index] == self._file_info

    def __init__(self, parent):
        self._parent = parent
        self._file_info = None
        GObject.Object.__init__(self)
        EchoObservers(self)

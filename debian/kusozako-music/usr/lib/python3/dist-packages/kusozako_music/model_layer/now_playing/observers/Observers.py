# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelLayerReady import DeltaModelLayerReady
from .QueueNext import DeltaQueueNext
from .QueuePrevious import DeltaQueuePrevious
from .QueueIndex import DeltaQueueIndex
from .MetadataChanged import DeltaMetadataChanged


class EchoObservers:

    def __init__(self, parent):
        DeltaModelLayerReady(parent)
        DeltaQueueNext(parent)
        DeltaQueuePrevious(parent)
        DeltaQueueIndex(parent)
        DeltaMetadataChanged(parent)

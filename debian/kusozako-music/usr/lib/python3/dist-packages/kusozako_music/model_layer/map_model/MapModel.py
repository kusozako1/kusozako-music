# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals
from .MapFunc import DeltaMapFunc


class DeltaMapModel(Gtk.MapListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _set_buffer(self, file_info, buffer):
        file_info.set_attribute_string("kusozako1::title", buffer["title"])
        file_info.set_attribute_string("kusozako1::artist", buffer["artist"])
        file_info.set_attribute_string("kusozako1::album", buffer["album"])

    def _signal_received(self, file_info, buffer):
        for item in self:
            if item == file_info:
                self._set_buffer(item, buffer)
                break

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.METADATA_CHANGED:
            return
        file_info, buffer = param
        self._signal_received(file_info, buffer)

    def _delta_info_map_model(self):
        return self

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MapListModel.__init__(self)
        DeltaMapFunc(self)
        self._raise("delta > register application object", self)

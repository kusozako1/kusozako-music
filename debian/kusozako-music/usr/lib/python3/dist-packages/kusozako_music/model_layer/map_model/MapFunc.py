# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.audio_metadata.AudioMetadata import FoxtrotAudioMetadata


class DeltaMapFunc(DeltaEntity):

    def _loaded(self, *args):
        file_info, title, artist, album, duration = args
        file_info.set_attribute_string("kusozako1::title", title)
        file_info.set_attribute_string("kusozako1::artist", artist)
        file_info.set_attribute_string("kusozako1::album", album)
        file_info.set_attribute_uint32("kusozako1::duration", duration)

    def _map_func(self, file_info, metadata):
        metadata.load(file_info, self._loaded)
        return file_info

    def __init__(self, parent):
        self._parent = parent
        metadata = FoxtrotAudioMetadata.get_default()
        map_model = self._enquiry("delta > map model")
        map_model.set_map_func(self._map_func, metadata)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ApplicationSignals
from kusozako_music.alfa.ApplicationObserver import AlfaApplicationObserver


class DeltaMetadataChanged(AlfaApplicationObserver):

    __signal__ = ApplicationSignals.METADATA_CHANGED

    def _signal_received(self, param):
        self._raise("delta > metadata changed", param)

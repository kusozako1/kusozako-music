# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ApplicationSignals
from kusozako_music.alfa.ApplicationObserver import AlfaApplicationObserver


class DeltaQueueIndex(AlfaApplicationObserver):

    __signal__ = ApplicationSignals.QUEUE_INDEX

    def _signal_received(self, index):
        model = self._enquiry("delta > viewer model")
        if index >= len(model):
            print("invalid index")
            return
        file_info = model[index]
        self._raise("delta > new file info", file_info)

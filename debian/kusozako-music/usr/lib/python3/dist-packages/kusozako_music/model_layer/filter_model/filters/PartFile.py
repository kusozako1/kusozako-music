# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaPartFile(DeltaEntity, Gtk.CustomFilter):

    def _match_func(self, file_info, user_data=None):
        name = file_info.get_name()
        if name.endswith(".part"):
            return False
        if name.endswith(".temp.m4a"):
            return False
        return True

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > add filter", self)

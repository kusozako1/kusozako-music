# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Spacer import DeltaSpacer
from .TitleLabel import DeltaTitleLabel
from .Description import DeltaDescription
from .content_area.ContentArea import DeltaContentArea
from .SpacerBottom import DeltaSpacerBottom


class DeltaWidgets(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaSpacer(self)
        DeltaTitleLabel(self)
        DeltaDescription(self)
        DeltaContentArea(self)
        DeltaSpacerBottom(self)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)

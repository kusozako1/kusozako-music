# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .CancelButton import DeltaCancelButton
from .ApplyButton import DeltaApplyButton

HEIGHT = 48


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def compute_position(self, overlay, rectangle):
        allocated_width = overlay.get_allocated_width()
        allocated_height = overlay.get_allocated_height()
        rectangle.x = 0
        rectangle.y = allocated_height-HEIGHT
        rectangle.width = allocated_width
        rectangle.height = HEIGHT
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=4,
            )
        DeltaCancelButton(self)
        DeltaApplyButton(self)
        self._raise("delta > add overlay", self)

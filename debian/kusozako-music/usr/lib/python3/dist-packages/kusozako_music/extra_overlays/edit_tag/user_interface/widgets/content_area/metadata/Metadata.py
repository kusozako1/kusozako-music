# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .rows.Rows import EchoRows


class DeltaMetadata(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, row):
        self.append(row)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=8,
            )
        self.append(Gtk.Box(vexpand=True))
        EchoRows(self)
        self.append(Gtk.Box(vexpand=True))
        self._raise("delta > add to container", self)

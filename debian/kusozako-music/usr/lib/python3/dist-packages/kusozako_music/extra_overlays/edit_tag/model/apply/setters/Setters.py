# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .MP4 import DeltaMP4
from .MP3 import DeltaMP3


class DeltaSetters(DeltaEntity):

    def get_setter_for_mime(self, mime):
        if mime in self._setters:
            return self._setters[mime]
        return None

    def __init__(self, parent):
        self._parent = parent
        self._setters = {
            "audio/mp4": DeltaMP4(self),
            "audio/mpeg": DeltaMP3(self)
            }

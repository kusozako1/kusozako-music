# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_music.const import ApplicationSignals
from .observers.Observers import EchoObservers
from .setters.Setters import DeltaSetters


class DeltaApply(DeltaEntity):

    def _delta_call_apply_changes(self):
        mime = self._file_info.get_content_type()
        setter = self._setters.get_setter_for_mime(mime)
        if setter is None:
            return
        gfile = self._file_info.get_attribute_object("standard::file")
        path = gfile.get_path()
        setter.apply_for_path(path)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _delta_call_metadata_changes_applied(self, buffer_):
        signal_param = self._file_info, buffer_
        user_data = ApplicationSignals.METADATA_CHANGED, signal_param
        self._raise("delta > application signal", user_data)

    def _delta_call_initialized(self, file_info):
        self._file_info = file_info

    def __init__(self, parent):
        self._parent = parent
        self._setters = DeltaSetters(self)
        EchoObservers(self)

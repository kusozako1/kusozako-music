# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaCancelButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        spacer = Gtk.Box(hexpand=True)
        container.append(spacer)
        Gtk.Button.__init__(
            self,
            label=_("Cancel"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=8,
            )
        self.set_size_request(120, -1)
        self.add_css_class("kusozako-primary-widget")
        container.append(self)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", container)

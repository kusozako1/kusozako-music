# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .SignalReceiver import DeltaSignalReceiver
from .user_interface.UserInterface import DeltaUserInterface
from .model.Model import DeltaModel


class DeltaEditTag(DeltaEntity):

    def _delta_call_dialog_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_dialog_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaModel(self)
        DeltaUserInterface(self)
        DeltaSignalReceiver(self)

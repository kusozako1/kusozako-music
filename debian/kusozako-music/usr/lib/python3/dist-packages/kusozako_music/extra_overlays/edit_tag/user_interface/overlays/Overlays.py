# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CloseButton import DeltaCloseButton
from .action_bar.ActionBar import DeltaActionBar


class EchoOverlays:

    def __init__(self, parent):
        DeltaCloseButton(parent)
        DeltaActionBar(parent)

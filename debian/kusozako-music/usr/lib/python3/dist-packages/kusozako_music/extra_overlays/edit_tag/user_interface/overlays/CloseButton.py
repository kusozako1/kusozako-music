# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals

SIZE = 32
MARGIN = 16


class DeltaCloseButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def compute_position(self, overlay, rectangle):
        allocated_width = overlay.get_allocated_width()
        rectangle.x = allocated_width-(SIZE+MARGIN)
        rectangle.y = MARGIN
        rectangle.width = SIZE
        rectangle.height = SIZE
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            icon_name="window-close-symbolic",
            )
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add overlay", self)

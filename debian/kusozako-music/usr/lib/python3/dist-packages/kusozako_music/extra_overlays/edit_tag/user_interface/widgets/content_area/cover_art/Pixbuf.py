# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals
from kusozako1.audio_cover_art.AudioCoverArt import FoxtrotAudioCoverArt

INTERP = GdkPixbuf.InterpType.BILINEAR


class DeltaPixbuf(DeltaEntity):

    def get_scaled_for_size(self, width, height):
        if self._pixbuf is None:
            return None
        horizontal_ratio = min(1, width/self._pixbuf.get_width())
        vertical_ratio = min(1, height/self._pixbuf.get_height())
        ratio = min(horizontal_ratio, vertical_ratio)
        new_width = self._pixbuf.get_width()*ratio
        new_height = self._pixbuf.get_height()*ratio
        return self._pixbuf.scale_simple(new_width, new_height, INTERP)

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != EditTagSignals.FILE_INFO_CHANGED:
            return
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        self._pixbuf = self._loader.get_for_uri(uri)
        self._raise("delta > pixbuf loaded")

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._loader = FoxtrotAudioCoverArt.new_for_maximum_size(300)
        self._raise("delta > register dialog object", self)

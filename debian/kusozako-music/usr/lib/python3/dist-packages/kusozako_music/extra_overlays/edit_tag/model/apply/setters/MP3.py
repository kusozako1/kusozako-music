
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3
from mutagen.mp4 import MP4Cover
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity

KEYS = {"TIT2": "title", "TPE1": "artist", "TALB": "album"}


class DeltaMP3(DeltaEntity):

    def _get_pixbuf(self, path, size):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = max(size/width, size/height)
        return pixbuf.scale_simple(
            width*scale,
            height*scale,
            GdkPixbuf.InterpType.HYPER
            )

    def _try_set_coverart(self, mutagen_file, path):
        if path is None:
            return
        pixbuf = self._get_pixbuf(path, 300)
        success, bytes_ = pixbuf.save_to_bufferv("png", [], [])
        if success:
            cover = MP4Cover(bytes_, MP4Cover.FORMAT_PNG)
            mutagen_file["covr"] = [cover]

    def _try_set_metadata(self, mutagen_file, buffer_):
        for mutagen_key, model_key in KEYS.items():
            model_value = buffer_[model_key]
            if not model_value:
                continue
            mutagen_file[model_key] = model_value
        self._raise("delta > metadata changes applied", buffer_)

    def apply_for_path(self, path):
        mp3_file = MP3(path, ID3=EasyID3)
        buffer_ = self._enquiry("delta > buffer")
        self._try_set_metadata(mp3_file, buffer_)
        # self._try_set_coverart(mutagen_file, buffer_["album-art"])
        mp3_file.save()

    def __init__(self, parent):
        self._parent = parent

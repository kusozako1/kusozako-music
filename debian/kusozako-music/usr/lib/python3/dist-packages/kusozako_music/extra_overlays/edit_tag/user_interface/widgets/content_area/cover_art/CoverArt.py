# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from .Pixbuf import DeltaPixbuf


class DeltaCoverArt(Gtk.DrawingArea, DeltaEntity):

    def _get_x_position(self, pixbuf, width):
        orientation = self._enquiry("delta > orientation")
        if orientation == Gtk.Orientation.HORIZONTAL:
            return width - pixbuf.get_width()
        return (width-pixbuf.get_width())/2

    def _draw_func(self, drawing_area, cairo_context, width, height):
        pixbuf = self._pixbuf.get_scaled_for_size(width, height)
        if pixbuf is None:
            return
        x = self._get_x_position(pixbuf, width)
        y = (height-pixbuf.get_height())/2
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def _delta_call_pixbuf_loaded(self):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(
            self,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.set_size_request(-1, 240)
        self.set_draw_func(self._draw_func)
        self._pixbuf = DeltaPixbuf(self)
        self._raise("delta > add to container", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .OrientationChanger import DeltaOrientationChanger
from .cover_art.CoverArt import DeltaCoverArt
from .metadata.Metadata import DeltaMetadata


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_info_box(self):
        return self

    def _delta_info_orientation(self):
        return self.props.orientation

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_orientation_changed(self, is_horizontal):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, margin_start=32, margin_end=32)
        DeltaOrientationChanger(self)
        DeltaCoverArt(self)
        DeltaMetadata(self)
        self._raise("delta > add to container", self)

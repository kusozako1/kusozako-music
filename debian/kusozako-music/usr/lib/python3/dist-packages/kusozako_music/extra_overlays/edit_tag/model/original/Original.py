# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals
from .observers.Observers import EchoObservers

KEYS = "album-art", "title", "artist", "album"


class DeltaOriginal(DeltaEntity):

    def has_changes(self):
        buffer_ = self._enquiry("delta > buffer")
        for key in KEYS:
            if buffer_[key] != self._original[key]:
                return True
        return False

    def _delta_call_metadata_changed(self):
        has_change = self.has_changes()
        user_data = EditTagSignals.HAS_CHANGES_CHANGED, has_change
        self._raise("delta > dialog signal", user_data)

    def _delta_call_metadata_initialized(self, metadata):
        self._original = metadata.copy()
        user_data = EditTagSignals.HAS_CHANGES_CHANGED, False
        self._raise("delta > dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._original = None
        EchoObservers(self)

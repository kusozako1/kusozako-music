# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.audio_metadata.AudioMetadata import FoxtrotAudioMetadata
from kusozako_music.const import EditTagSignals
from .observers.Observers import EchoObservers

DATA = {"album-art": None, "title": None, "artist": None, "album": None}


class DeltaBuffer(DeltaEntity):

    def _metadata_loaded(self, title, artist, album, duration, file_info):
        self._buffer["title"] = title
        self._buffer["artist"] = artist
        self._buffer["album"] = album
        user_data = EditTagSignals.METADATA_INITIALIZED, self._buffer.copy()
        self._raise("delta > dialog signal", user_data)

    def _delta_call_metadata_changed(self, user_data):
        key, value = user_data
        self._buffer[key] = value

    def _delta_call_file_info_changed(self, file_info):
        self._buffer = DATA.copy()
        self._metadata.load_async(file_info, self._metadata_loaded, file_info)

    def get_buffer(self):
        return self._buffer

    def __init__(self, parent):
        self._parent = parent
        self._buffer = DATA.copy()
        self._metadata = FoxtrotAudioMetadata.get_default()
        EchoObservers(self)

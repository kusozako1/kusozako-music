# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import DBusSpec
from .control.Control import DeltaControl


class DeltaDBus(DeltaEntity):

    def _on_name_aquired(self, dbus_connection, name):
        DeltaControl.new(self, dbus_connection)

    def _on_bus_get(self, cancellable, task):
        dbus_connection = Gio.bus_get_finish(task)
        Gio.bus_own_name_on_connection(
            dbus_connection,
            DBusSpec.WELL_KNOWN_NAME_PLAYER,
            Gio.BusNameOwnerFlags.NONE,
            self._on_name_aquired
            )

    def __init__(self, parent):
        self._parent = parent
        Gio.bus_get(Gio.BusType.SESSION, None, self._on_bus_get)

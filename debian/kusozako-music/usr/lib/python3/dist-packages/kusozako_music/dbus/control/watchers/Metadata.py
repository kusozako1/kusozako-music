# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaMetadata(DeltaEntity):

    def _on_notify(self, now_playing, file_info):
        title = file_info.get_attribute_string("kusozako1::title")
        artist = file_info.get_attribute_string("kusozako1::artist")
        album = file_info.get_attribute_string("kusozako1::album")
        signal_param = GLib.Variant.new_tuple(
            GLib.Variant.new_string(title),
            GLib.Variant.new_string(artist),
            GLib.Variant.new_string(album)
            )
        user_data = "MetadataChanged", signal_param
        self._raise("delta > emit signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify)

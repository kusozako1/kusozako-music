# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ApplicationSignals
from .Method import AlfaMethod


class DeltaPlay(AlfaMethod):

    __method_name__ = "Play"

    def _invoke(self, param, invocation):
        user_data = ApplicationSignals.PLAY_PLAYBACK, None
        self._raise("delta > application signal", user_data)
        invocation.return_value(None)

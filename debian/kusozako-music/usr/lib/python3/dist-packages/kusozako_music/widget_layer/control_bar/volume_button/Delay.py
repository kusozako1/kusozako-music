
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

DELAY = 5


class DeltaDelay(DeltaEntity):

    def _timeout(self, index):
        if index == self._index:
            self._raise("delta > popdown")
        return GLib.SOURCE_REMOVE

    def start_idle(self):
        index = self._index+1
        GLib.timeout_add_seconds(DELAY, self._timeout, index)
        self._index += 1

    def __init__(self, parent):
        self._parent = parent
        self._index = 0

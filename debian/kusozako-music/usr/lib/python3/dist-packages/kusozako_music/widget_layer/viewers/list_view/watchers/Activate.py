# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaActivate(DeltaEntity):

    def _on_event(self, viewer, index):
        user_data = ApplicationSignals.QUEUE_INDEX, index
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        viewer.connect("activate", self._on_event)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaDrag(Gtk.GestureDrag, DeltaEntity):

    def _on_drag_begin(self, gesture, start_x, start_y):
        self._start_y = start_y

    def _on_drag_update(self, gesture, offset_x, offset_y):
        if self._start_y is None:
            return
        y = self._start_y+offset_y
        volume = min(100, max(0, 132-y-16))
        self._raise("delta > change ratio", volume/100)

    def __init__(self, parent):
        self._parent = parent
        self._start_y = None
        Gtk.GestureDrag.__init__(self)
        self.connect("drag-begin", self._on_drag_begin)
        self.connect("drag-update", self._on_drag_update)
        self._raise("delta > add controller", self)

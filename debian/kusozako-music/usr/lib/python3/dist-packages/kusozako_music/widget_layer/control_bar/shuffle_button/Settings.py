# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import PlaybackMode
from kusozako_music.const.Settings import PLAYBACK_MODE
from kusozako_music.const.Settings import PLAYBACK_MODE_DEFAULT

ICONS = {
    PlaybackMode.REPEAT_ALL: "media-playlist-repeat-symbolic",
    PlaybackMode.REPEAT_ONE: "media-playlist-repeat-song-symbolic",
    PlaybackMode.SHUFFLE: "media-playlist-shuffle-symbolic",
    }

TOOLTIP_TEXT = {
    PlaybackMode.REPEAT_ALL: _("Repeat All"),
    PlaybackMode.REPEAT_ONE: _("Repeat One"),
    PlaybackMode.SHUFFLE: _("Shuffle"),
}


class DeltaSettings(DeltaEntity):

    def get_icon_name(self):
        return ICONS[self._mode]

    def get_tooltip_text(self):
        return TOOLTIP_TEXT[self._mode]

    def change_mode(self):
        new_mode = (self._mode+1) % PlaybackMode.N_MODE
        user_data = PLAYBACK_MODE + (new_mode,)
        self._raise("delta > settings", user_data)

    def receive_transmission(self, user_data):
        group, key, mode = user_data
        if group != "playback" or key != "mode":
            return
        self._mode = mode
        user_data = TOOLTIP_TEXT[self._mode], ICONS[self._mode]
        self._raise("delta > mode changed", user_data)

    def __init__(self, parent):
        self._parent = parent
        query = PLAYBACK_MODE_DEFAULT
        self._mode = self._enquiry("delta > settings", query)
        self._raise("delta > register settings object", self)

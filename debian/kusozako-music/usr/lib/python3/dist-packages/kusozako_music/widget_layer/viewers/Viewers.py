# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ViewerTypes
from .home.Home import DeltaHome
from .grid_view.GridView import DeltaGridView
from .list_view.ListView import DeltaListView
from .observers.Observers import EchoObservers


class DeltaViewers(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _delta_info_visible_child_name(self):
        return self.get_visible_child_name()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            )
        DeltaHome(self)
        DeltaGridView(self)
        DeltaListView(self)
        EchoObservers(self)
        query = "viewer", "type", ViewerTypes.HOME
        visible_name = self._enquiry("delta > settings", query)
        self.set_visible_child_name(visible_name)
        self._raise("delta > add to container", self)

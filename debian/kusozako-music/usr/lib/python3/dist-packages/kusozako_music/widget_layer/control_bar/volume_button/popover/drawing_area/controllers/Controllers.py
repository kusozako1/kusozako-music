# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Drag import DeltaDrag
from .Click import DeltaClick


class EchoControllers:

    def __init__(self, parent):
        DeltaDrag(parent)
        DeltaClick(parent)

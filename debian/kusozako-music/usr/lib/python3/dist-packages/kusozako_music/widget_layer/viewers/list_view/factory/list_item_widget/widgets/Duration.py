# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals
from kusozako_music.util import ReadableDuration


class DeltaDuration(Gtk.Label, DeltaEntity):

    def _idle(self, total_seconds):
        label = ReadableDuration.from_seconds(total_seconds)
        self.set_label(label)

    def _bind(self, file_info):
        total_seconds = file_info.get_attribute_uint32("kusozako1::duration")
        GLib.idle_add(self._idle, total_seconds)

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != ListItemSignals.FILE_INFO_CHANGED:
            return
        self._bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, xalign=1, margin_end=8)
        self._raise("delta > add to container", self)
        self._raise("delta > register list item object", self)

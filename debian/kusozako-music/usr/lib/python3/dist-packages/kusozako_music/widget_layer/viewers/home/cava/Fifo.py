# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

PATH = "/tmp/kusozako-music.fifo"


def _wait(subprocess, task):
    _ = subprocess.wait_finish(task)
    print("exists ?", subprocess.get_exit_status())


def _make_fifo():
    command = ["mkfifo", PATH]
    subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
    subprocess.wait_async(None, _wait)


def ensure():
    if not GLib.file_test(PATH, GLib.FileTest.EXISTS):
        _make_fifo()
    return PATH

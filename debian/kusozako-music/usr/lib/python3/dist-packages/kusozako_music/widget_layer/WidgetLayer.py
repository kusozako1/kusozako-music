# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals
from .search_bar.SearchBar import DeltaSearchBar
from .viewers.Viewers import DeltaViewers
from .control_bar.ControlBar import DeltaControlBar


class DeltaWidgetLayer(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _delta_call_add_to_overlay(self, widget):
        self.add_overlay(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        if widget.__class__.__name__ == "DeltaControlBar":
            height = overlay.get_allocated_height()
            rectangle.y = height-40
            rectangle.height = 40
        else:
            rectangle.y = 0
            rectangle.height = 40
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self, hexpand=True, vexpand=True)
        self.connect("get-child-position", self._on_get_child_position)
        DeltaSearchBar(self)
        DeltaViewers(self)
        DeltaControlBar(self)
        self._raise("delta > add to container", self)
        user_data = ApplicationSignals.WIDGET_LAYER_READY, None
        self._raise("delta > application signal", user_data)

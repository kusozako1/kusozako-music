# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaClick(Gtk.GestureClick, DeltaEntity):

    def _on_pressed(self, gesture, n_press, x, y):
        volume = min(100, max(0, 132-y-16))
        self._raise("delta > change ratio", volume/100)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self)
        self.connect("pressed", self._on_pressed)
        self._raise("delta > add controller", self)

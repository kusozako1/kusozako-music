# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import struct
import threading
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

BYTE_TYPE = "H"                 # unsigned short integer
BYTE_SIZE = 2                   # uses 16 bit format


class DeltaBufferReader(DeltaEntity):

    @classmethod
    def new(cls, parent, fifo_path, n_bars):
        instance = cls(parent)
        instance.construct(fifo_path, n_bars)
        return instance

    def _target(self, buffer_reader, sample_size, format_):
        while not self._cancelled:
            buffer = buffer_reader.read(sample_size)
            if sample_size > len(buffer):
                break
            unpacked_buffer = struct.unpack(format_, buffer)
            sample = [level / GLib.MAXUSHORT for level in unpacked_buffer]
            self._raise("delta > sample", sample.copy())

    def kill(self):
        self._cancelled = True

    def construct(self, fifo_path, n_bars):
        buffer_reader = open(fifo_path, "rb")
        sample_size = BYTE_SIZE*n_bars
        format_ = BYTE_TYPE*n_bars
        thread_args = (buffer_reader, sample_size, format_)
        thread = threading.Thread(target=self._target, args=thread_args)
        thread.start()

    def __init__(self, parent):
        self._parent = parent
        self._cancelled = False

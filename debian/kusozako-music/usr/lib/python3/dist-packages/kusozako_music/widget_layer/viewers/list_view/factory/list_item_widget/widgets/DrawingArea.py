# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals

SIZE = 64


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._pixbuf is None:
            return
        x = (width-self._pixbuf.get_width())/2
        y = min(16, (height-self._pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, self._pixbuf, x, y)
        cairo_context.paint()

    def _loaded(self, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(SIZE/width, SIZE/height)
        new_width = int(width*scale)
        new_height = int(height*scale)
        self._pixbuf = pixbuf.scale_simple(new_width, new_height, 2)
        self.queue_draw()

    def receive_transmission(self, user_data):
        signal, pixbuf = user_data
        if signal != ListItemSignals.COVERART_CHANGED:
            return
        self._loaded(pixbuf)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self.set_size_request(SIZE, SIZE)
        self._raise("delta > add to container", self)
        self._raise("delta > register list item object", self)

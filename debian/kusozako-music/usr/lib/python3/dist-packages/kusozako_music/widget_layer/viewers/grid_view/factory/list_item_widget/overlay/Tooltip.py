# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.alfa.tooltip.Tooltip import AlfaTooltip


class DeltaTooltip(AlfaTooltip):

    __source_widget_query__ = "delta > overlay"

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def refresh(self):
        label = self._enquiry("delta > label")
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, label="")
        self.add_css_class("kusozako-text-backlight")
        self._raise("delta > add to overlay", self)

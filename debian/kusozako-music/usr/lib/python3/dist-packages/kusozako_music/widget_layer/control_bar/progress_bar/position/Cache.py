# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.Progress import FoxtrotProgress


class DeltaCache(DeltaEntity):

    def _is_valid(self):
        if 0 > self._position or 0 >= self._duration:
            False
        return True

    def _on_changed(self, progress, position, duration):
        self._position = position
        self._duration = duration
        self._ratio = 0 if not self._is_valid else position/duration
        self._raise("delta > position changed")

    def get_ratio(self):
        return self._ratio

    def get_cache(self):
        return self._is_valid(), self._position, self._duration

    def __init__(self, parent):
        self._parent = parent
        self._ratio = 0
        self._position = -1
        self._duration = -1
        progress = FoxtrotProgress.get_default()
        progress.connect("changed", self._on_changed)

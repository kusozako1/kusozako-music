# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .label.Label import DeltaLabel
from .drawing_area.DrawingArea import DeltaDrawingArea
from .position.Position import DeltaPosition
from .Motion import DeltaMotion
from .Click import DeltaClick


class DeltaProgressBar(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _delta_call_add_to_overlay(self, widget):
        self.add_overlay(widget)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_info_motion_ratio(self):
        return self._motion.get_ratio()

    def _delta_info_label(self):
        return self._position.get_label()

    def _delta_info_position(self):
        return self._position.get_position()

    def _delta_call_position_changed(self):
        self._drawing_area.queue_draw()
        self._label.refresh()

    def __init__(self, parent):
        self._parent = parent
        self._position = DeltaPosition(self)
        Gtk.Overlay.__init__(self, hexpand=True)
        self._label = DeltaLabel(self)
        self._motion = DeltaMotion(self)
        DeltaClick(self)
        self._drawing_area = DeltaDrawingArea(self)
        self._raise("delta > add to container", self)

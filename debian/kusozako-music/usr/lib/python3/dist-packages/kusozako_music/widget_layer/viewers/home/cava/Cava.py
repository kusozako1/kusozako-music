# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .DrawingArea import DeltaDrawingArea
from .Subprocess import FoxtrotSubprocess
from .BufferReader import DeltaBufferReader
from . import Fifo

N_BARS = 30


class DeltaCava(DeltaEntity):

    def _delta_call_sample(self, sample):
        self._drawing_area.set_sample(sample)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.ABOUT_TO_CLOSE:
            return
        self._buffer_reader.kill()
        self._subprocess.kill()

    def __init__(self, parent):
        self._parent = parent
        self._drawing_area = DeltaDrawingArea(self)
        fifo_path = Fifo.ensure()
        self._subprocess = FoxtrotSubprocess(fifo_path, N_BARS)
        self._buffer_reader = DeltaBufferReader.new(self, fifo_path, N_BARS)
        self._raise("delta > register main window signal object", self)

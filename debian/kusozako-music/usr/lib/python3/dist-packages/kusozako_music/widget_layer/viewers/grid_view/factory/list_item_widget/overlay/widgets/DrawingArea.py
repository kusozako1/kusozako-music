# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._pixbuf is None:
            return
        x = (width-self._pixbuf.get_width())/2
        y = min(16, (height-self._pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, self._pixbuf, x, y)
        cairo_context.paint()

    def receive_transmission(self, user_data):
        signal, pixbuf = user_data
        if signal != ListItemSignals.COVERART_CHANGED:
            return
        self._pixbuf = pixbuf
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self.set_size_request(128, 128)
        self._raise("delta > register list item object", self)
        self._raise("delta > add to container", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _on_leave(self, motion):
        self._ratio = 0
        self._raise("delta > position changed")

    def _on_motion(self, motion, x, y):
        widget = motion.get_widget()
        self._ratio = x / widget.get_allocated_width()
        self._raise("delta > position changed")

    def get_ratio(self):
        return self._ratio

    def __init__(self, parent):
        self._parent = parent
        self._ratio = 0
        Gtk.EventControllerMotion.__init__(self)
        self.connect("leave", self._on_leave)
        self.connect("motion", self._on_motion)
        self._raise("delta > add controller", self)

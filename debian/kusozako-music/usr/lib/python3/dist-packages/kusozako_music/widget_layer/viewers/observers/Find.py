# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals
from kusozako_music.const import ViewerTypes


class DeltaFind(DeltaEntity):

    def _try_switch(self):
        visible_child_name = self._enquiry("delta > visible child name")
        if visible_child_name != ViewerTypes.HOME:
            return
        settings = "viewer", "type", ViewerTypes.GRID_VIEW
        self._raise("delta > settings", settings)

    def receive_transmission(self, user_data):
        signal, child_revealed = user_data
        if signal != ApplicationSignals.SEARCH_BAR_TOGGLED:
            return
        if not child_revealed:
            return
        self._try_switch()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)

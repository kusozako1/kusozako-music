# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals


class DeltaCheckButton(Gtk.CheckButton, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, selected = user_data
        if signal != ListItemSignals.SELECTION_CHANGED:
            return
        self.set_active(selected)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CheckButton.__init__(self, margin_start=4)
        self.set_size_request(32, -1)
        self.add_css_class("selection-mode")
        self._raise("delta > add to container", self)
        self._raise("delta > register list item object", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals


class DeltaInfo(Gtk.Box, DeltaEntity):

    def _bind(self, file_info):
        title = file_info.get_attribute_string("kusozako1::title")
        self._primary.set_text(title)
        artist = file_info.get_attribute_string("kusozako1::artist")
        album = file_info.get_attribute_string("kusozako1::album")
        secondary_text = "{} / {}".format(artist, album)
        self._secondary.set_text(secondary_text)

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != ListItemSignals.FILE_INFO_CHANGED:
            return
        self._bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            margin_start=4,
            )
        self._primary = Gtk.Inscription(
            nat_lines=2,
            hexpand=True,
            vexpand=True,
            text_overflow=Gtk.InscriptionOverflow.ELLIPSIZE_END,
            )
        self.append(self._primary)
        self._secondary = Gtk.Label(
            xalign=0,
            ellipsize=Pango.EllipsizeMode.END,
            )
        self._secondary.add_css_class("dim-label")
        self.append(self._secondary)
        self._raise("delta > add to container", self)
        self._raise("delta > register list item object", self)

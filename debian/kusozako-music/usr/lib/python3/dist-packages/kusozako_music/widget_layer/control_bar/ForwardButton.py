# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaForwardButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ApplicationSignals.QUEUE_NEXT, None
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            icon_name="media-seek-forward-symbolic",
            tooltip_text=_("Forward"),
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

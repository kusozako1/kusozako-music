# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako1.Entity import DeltaEntity
from .Cache import DeltaCache


class DeltaPosition(DeltaEntity):

    def _get_readable(self, total_seconds):
        hours = total_seconds // 3600
        minutes = total_seconds % 3600 // 60
        seconds = total_seconds % 60
        if hours == 0:
            return "{}:{:02}".format(minutes, seconds)
        return "{}:{:02}:{:02}".format(hours, minutes, seconds)

    def _get_position(self, position, duration):
        motion_ratio = self._enquiry("delta > motion ratio")
        if motion_ratio == 0:
            unreadable = int(position/Gst.SECOND)
        else:
            unreadable = int(duration*motion_ratio/Gst.SECOND)
        return self._get_readable(unreadable)

    def get_label(self):
        valid, position, duration = self._cache.get_cache()
        if not valid:
            return ""
        return "{} / {}".format(
            self._get_position(position, duration),
            self._get_readable(int(duration/Gst.SECOND))
            )

    def get_position(self):
        return self._cache.get_ratio()

    def __init__(self, parent):
        self._parent = parent
        self._cache = DeltaCache(self)

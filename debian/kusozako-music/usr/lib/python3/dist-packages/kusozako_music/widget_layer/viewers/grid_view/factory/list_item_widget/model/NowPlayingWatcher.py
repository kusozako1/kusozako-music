# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals


class DeltaNowPlayingWatcher(DeltaEntity):

    @classmethod
    def new(cls, parent, now_playing):
        instance = cls(parent)
        instance.construct(now_playing)
        return instance

    def _on_notify(self, now_playing, file_info):
        if file_info is None:
            return
        selected = (file_info == self._file_info)
        user_data = ListItemSignals.SELECTION_CHANGED, selected
        self._raise("delta > list item signal", user_data)

    def _on_metadata_changed(self, selection_model, file_info, buffer):
        if file_info != self._file_info:
            return
        file_info.set_attribute_string("kusozako1::title", buffer["title"])
        file_info.set_attribute_string("kusozako1::artist", buffer["artist"])
        file_info.set_attribute_string("kusozako1::album", buffer["album"])
        user_data = ListItemSignals.FILE_INFO_CHANGED, file_info
        self._raise("delta > list item signal", user_data)

    def bind(self, selected, file_info):
        self._file_info = file_info
        user_data = ListItemSignals.SELECTION_CHANGED, selected
        self._raise("delta > list item signal", user_data)

    def construct(self, now_playing):
        now_playing.connect("now-playing-changed", self._on_notify)
        now_playing.connect("metadata-changed", self._on_metadata_changed)

    def __init__(self, parent):
        self._parent = parent
        self._file_info = None

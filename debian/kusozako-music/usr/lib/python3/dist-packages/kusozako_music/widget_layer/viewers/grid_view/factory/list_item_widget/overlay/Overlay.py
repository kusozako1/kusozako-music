# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Tooltip import DeltaTooltip
from .widgets.Widgets import EchoWidgets
from .GestureClick import DeltaGestureClick


class DeltaOverlay(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_info_overlay(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(
            self,
            overflow=Gtk.Overflow.HIDDEN,
            has_tooltip=True,
            )
        self.add_css_class("card")
        DeltaTooltip(self)
        EchoWidgets(self)
        DeltaGestureClick(self)
        self._raise("delta > add to container", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

TEMPLATE = {
    "title": "<span font-size='xx-large' weight='normal' >{}</span>\n\n",
    "artist": "<span font-size='large'>Artist : {}\n\n</span>",
    "album": "<span font-size='large'>Album : {}</span>",
    }

ATTRIBUTE = {
    "title": "kusozako1::title",
    "artist": "kusozako1::artist",
    "album": "kusozako1::album",
    }


class FoxtrotMarkup:

    def _build_line(self, key, file_info):
        attribute_name = ATTRIBUTE[key]
        text = file_info.get_attribute_string(attribute_name)
        return self._build_text(key, text)

    def _build_text(self, key, unescaped_text):
        if not unescaped_text:
            return ""
        escaped_text = GLib.markup_escape_text(unescaped_text, -1)
        template = TEMPLATE[key]
        return template.format(escaped_text, -1)

    def build_for_file_info(self, file_info):
        markup = self._build_line("title", file_info)
        markup += self._build_line("artist", file_info)
        markup += self._build_line("album", file_info)
        return markup

    def build_for_buffer(self, buffer):
        markup = self._build_text("title", buffer["title"])
        markup += self._build_text("artist", buffer["artist"])
        markup += self._build_text("album", buffer["album"])
        return markup

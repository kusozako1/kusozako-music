# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .StatusLabel import DeltaStatusLabel
from .NameLabel import DeltaNameLabel
from .DrawingArea import DeltaDrawingArea


class EchoWidgets:

    def __init__(self, parent):
        DeltaDrawingArea(parent)
        DeltaStatusLabel(parent)
        DeltaNameLabel(parent)

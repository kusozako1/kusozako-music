# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi

gi.require_version('Gst', '1.0')

VERSION = "2025.02.01"
APPLICATION_NAME = "kusozako-music"
APPLICATION_ID = "com.gitlab.kusozako1.Music"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 0,
    "use-global-find": True,
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": "Music Player for kusozako project"
}

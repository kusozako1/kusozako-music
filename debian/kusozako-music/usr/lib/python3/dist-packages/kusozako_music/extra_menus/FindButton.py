# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_music.const import ApplicationSignals


class DeltaFindButton(AlfaButton):

    START_ICON = "edit-find-symbolic"
    LABEL = _("Find")
    SHORTCUT = "Ctrl+F"

    def _on_clicked(self, button):
        user_data = ApplicationSignals.TOGGLE_SEARCH_BAR, None
        self._raise("delta > application signal", user_data)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.EDIT_FIND:
            return
        user_data = ApplicationSignals.TOGGLE_SEARCH_BAR, None
        self._raise("delta > application signal", user_data)

    def _set_to_container(self):
        user_data = MainWindowSignals.ADD_EXTRA_MENU, self
        self._raise("delta > main window signal", user_data)
        param = "Ctrl+F", MainWindowSignals.EDIT_FIND, None
        user_data = MainWindowSignals.ADD_GLOBAL_ACCEL, param
        self._raise("delta > main window signal", user_data)
        self._raise("delta > register main window signal object", self)
        param = _("Music"), "<Ctrl>f", _("Find")
        user_data = MainWindowSignals.ADD_SHORTCUT, param
        self._raise("delta > main window signal", user_data)

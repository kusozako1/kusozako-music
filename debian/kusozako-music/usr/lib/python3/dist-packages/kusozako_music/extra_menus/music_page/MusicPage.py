# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako1.overlay_item.Separator import DeltaSeparator
from .control_buttons.ControlButtons import DeltaControlButtons
from .repeat_mode.RepeatMode import EchoRepeatMode
from .view_type.ViewType import EchoViewType


class DeltaMusicPage(AlfaSubPage):

    PAGE_NAME = "music"

    def _on_initialize(self):
        DeltaControlButtons(self)
        DeltaSeparator(self)
        EchoRepeatMode(self)
        DeltaSeparator(self)
        EchoViewType(self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FindButton import DeltaFindButton
from .MusicButton import DeltaMusicButton
from .music_page.MusicPage import DeltaMusicPage


class EchoExtraMenus:

    def __init__(self, parent):
        DeltaFindButton(parent)
        DeltaMusicButton(parent)
        DeltaMusicPage(parent)

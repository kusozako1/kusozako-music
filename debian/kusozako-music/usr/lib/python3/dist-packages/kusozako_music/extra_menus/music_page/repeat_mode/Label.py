# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    @classmethod
    def new_for_label(cls, parent, label_text):
        label = cls(parent)
        label.props.label = label_text

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            margin_start=8,
            margin_top=8,
            margin_bottom=8,
            xalign=0
            )
        self.add_css_class("body")
        self._raise("delta > add to container", self)

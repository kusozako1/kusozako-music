# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .BackwardButton import DeltaBackwardButton
from .ToggleButton import DeltaToggleButton
from .ForwardButton import DeltaForwardButton


class DeltaControlButtons(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaBackwardButton(self)
        DeltaToggleButton(self)
        DeltaForwardButton(self)
        self._raise("delta > add to container", self)

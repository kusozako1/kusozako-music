# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gst
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaToggleButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ApplicationSignals.TOGGLE_PlAYBACK, None
        self._raise("delta > application signal", user_data)

    def _on_transmission_received(self, state):
        if state == Gst.State.PLAYING:
            self.props.icon_name = "media-playback-pause-symbolic"
            self.props.tooltip_text = _("Pause")
        else:
            self.props.icon_name = "media-playback-start-symbolic"
            self.props.tooltip_text = _("Play")

    def receive_transmission(self, user_data):
        signal, state = user_data
        if signal != ApplicationSignals.PLAYBACK_STATE_CHANGED:
            return
        self._on_transmission_received(state)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            icon_name="media-playback-start-symbolic",
            tooltip_text=_("Play"),
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            hexpand=True,
            )
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ViewerTypes
from .ViewTypeButton import AlfaViewTypeButton


class DeltaHome(AlfaViewTypeButton):

    LABEL = _("Home")
    MATCH_VALUE = ViewerTypes.HOME

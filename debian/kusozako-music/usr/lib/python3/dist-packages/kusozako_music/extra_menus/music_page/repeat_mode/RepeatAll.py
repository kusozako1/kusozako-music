# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import PlaybackMode
from .RepeatModeButton import AlfaRepeatModeButton


class DeltaRepeatAll(AlfaRepeatModeButton):

    LABEL = _("Repeat All")
    MATCH_VALUE = PlaybackMode.REPEAT_ALL

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from .Home import DeltaHome
from .IconView import DeltaIconView
from .ListView import DeltaListView


class EchoViewType:

    def __init__(self, parent):
        DeltaLabel.new_for_label(parent, _("View Type"))
        DeltaHome(parent)
        DeltaIconView(parent)
        DeltaListView(parent)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import GObject
from gi.repository import GLib


class FoxtrotProgress(GObject.Object):

    __gsignals__ = {
        "changed": (GObject.SIGNAL_RUN_LAST, None, (float, float))
        }

    @classmethod
    def init_for_playbin(cls, playbin):
        if "default_instance" not in dir(cls):
            cls.default_instance = cls()
        cls.default_instance.construct(playbin)

    @classmethod
    def get_default(cls):
        if "default_instance" not in dir(cls):
            cls.default_instance = cls()
        return cls.default_instance

    def _timeout(self, playbin):
        _, position = playbin.query_position(Gst.Format.TIME)
        _, duration = playbin.query_duration(Gst.Format.TIME)
        user_data = (position, duration)
        self.emit("changed", *user_data)
        return GLib.SOURCE_CONTINUE

    def construct(self, playbin):
        GLib.timeout_add_seconds(1, self._timeout, playbin)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaApplicationObserver(DeltaEntity):

    __signal__ = "define receiving signal here."

    def _signal_received(self, param=None):
        raise NotImplementedError()

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != self.__signal__:
            return
        self._signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals


class DeltaCoverartChanged(DeltaEntity):

    __signal__ = ListItemSignals.COVERART_CHANGED

    def receive_transmission(self, user_data):
        signal, pixbuf = user_data
        if signal != self.__signal__:
            return
        self._raise("delta > pixbuf changed", pixbuf)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register list item object", self)

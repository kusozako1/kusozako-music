# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaVerticalAdjustment(DeltaEntity):

    def _idle(self, list_item_widget):
        viewer = self._enquiry("delta > viewer")
        success, rect = viewer.compute_bounds(list_item_widget)
        if not success:
            return
        widget_bottom = rect.get_y()*-1 + list_item_widget.get_height()
        bottom_margin = viewer.get_height() - widget_bottom
        if bottom_margin >= 48:
            return
        vadjustment = viewer.get_vadjustment()
        vadjustment.props.value += (48 - bottom_margin)

    def _scroll_to(self, list_item, index):
        # to wait redraw
        # Gdk.PRIORITY_REDRAW = 120
        # GLib.PRIORITY_DEFAULT_IDLE = 200¶
        viewer = self._enquiry("delta > viewer")
        viewer.scroll_to(index, Gtk.ListScrollFlags.NONE, None)
        list_item_widget = list_item.get_child()
        if list_item_widget is None:
            print("list_item is empty ?")
            return
        GLib.idle_add(self._idle, list_item_widget)

    def _on_selected(self, model, param_spec, list_item):
        current_index = model.get_selected()
        if current_index == list_item.get_position():
            self._scroll_to(list_item, current_index)

    def _on_notify(self, now_playing, file_info, list_item):
        current_index = now_playing.get_current_index()
        if list_item.get_position() == current_index:
            self._scroll_to(list_item, current_index)

    def connect(self, list_item):
        viewer_model = self._enquiry("delta > viewer model")
        viewer_model.connect("notify::selected", self._on_selected, list_item)
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify, list_item)
        return now_playing

    def __init__(self, parent):
        self._parent = parent

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math
from gi.repository import Pango
from gi.repository import PangoCairo
from kusozako_music.util import GtkFontDescription

MARGIN = 4
TRIANGLE_HEIGHT = 128*0.6
TRIANGLE_WIDTH = math.sqrt((TRIANGLE_HEIGHT*TRIANGLE_HEIGHT)*2)


class FoxtrotLayoutFactory:

    @classmethod
    def get_default(cls):
        if "_layout_factory" not in dir(cls):
            cls._layout_factory = cls()
        return cls._layout_factory

    def get_triangle(self, cairo_context):
        yuki_layout = PangoCairo.create_layout(cairo_context)
        yuki_layout.set_alignment(Pango.Alignment.CENTER)
        yuki_layout.set_font_description(self._font_description)
        yuki_layout.set_width(TRIANGLE_WIDTH*Pango.SCALE)
        yuki_layout.set_height(-1)
        yuki_layout.set_wrap(Pango.WrapMode.WORD_CHAR)
        yuki_layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        return yuki_layout.copy()

    def build_(self, cairo_context, width, max_height):
        layout = PangoCairo.create_layout(cairo_context)
        layout.set_alignment(Pango.Alignment.CENTER)
        layout.set_font_description(self._font_description)
        layout.set_width((width-MARGIN*2)*Pango.SCALE)
        layout.set_height(max_height*Pango.SCALE)
        layout.set_wrap(Pango.WrapMode.WORD_CHAR)
        layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        return layout.copy()

    def __init__(self):
        self._font_description = GtkFontDescription.get_description()

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


def from_seconds(total_seconds):
    hours = total_seconds // 3600
    minutes = total_seconds % 3600 // 60
    seconds = total_seconds % 60
    if hours == 0:
        return "{}:{:02}".format(minutes, seconds)
    else:
        return "{}:{:02}:{:02}".format(hours, minutes, seconds)

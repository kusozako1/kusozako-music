# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILE_INFO_CHANGED = "file-info-changed"         # Gio.FileInfo
METADATA_CHANGED = "metadata-changed"           # Gio.FileInfo
SELECTION_CHANGED = "selection-changed"         # bool
COVERART_CHANGED = "coverart-changed"           # GdkPixbuf.Pixbuf

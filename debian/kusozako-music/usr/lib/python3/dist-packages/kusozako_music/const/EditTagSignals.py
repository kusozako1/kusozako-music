# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILE_INFO_CHANGED = "file-info-changed"         # file_info
METADATA_INITIALIZED = "metadata-initialized"   # (title, artist, album)
METADATA_CHANGED = "metadata-changed"           # (key, value)
HAS_CHANGES_CHANGED = "has-changes-changed"     # boolean has-change is True
APPLY_CHANGES = "apply-changes"                 # None

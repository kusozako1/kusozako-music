# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PLAY_PLAYBACK = "play-playback"                     # None
PLAYBACK_PAUSE = "playback-pause"                   # None
TOGGLE_PlAYBACK = "toggle-playback"                 # None
FORWARD_QUEUE = "forward-queue"                     # None
BACKWARD_QUEUE = "backward-queue"                   # None
PLAYBACK_STATE_CHANGED = "playback-state-changed"   # state as Gst.State
PLAYBACK_SEEK_BY_RATIO = "playback-seek-by-ratio"   # float (0 to 1)
PLAYBACK_REWIND = "playback-rewind"                 # None
METADATA_CHANGED = "metadata-changed"               # gfile, metadata
ALBUM_ART_CHANGED = "album-art-changed"             # gfile
TOGGLE_SEARCH_BAR = "toggle-search-bar"             # None
SEARCH_BAR_TOGGLED = "search-bar-toggled"           # bool
SEARCH_KEYWORD_CHANGED = "search-keyword-changed"   # keyword as str

# EXPERIMENTAL
INDEX_ACTIVATED = "index-activated"                 # int
WIDGET_LAYER_READY = "widget-layer-ready"           # None
MODEL_LAYER_READY = "model-layer-ready"             # None
QUEUE_NEXT = "queue-next"                           # None
QUEUE_PREVIOUS = "queue-previous"                   # None
QUEUE_INDEX = "queue-index"                         # int

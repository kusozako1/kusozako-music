# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

REPEAT_ONE = 0
REPEAT_ALL = 1
SHUFFLE = 2
N_MODE = 3

# NEXT_MODE = (CURRENT_MODE+1) % N_MODE

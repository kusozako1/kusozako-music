#!/usr/bin/env python3

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.MainLoop import DeltaMainLoop


if __name__ == "__main__":
    DeltaMainLoop()

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from kusozako_music.const import DBusSpec
from .methods.Methods import EchoMethods
from .watchers.Watchers import EchoWatchers


class DeltaControl(DeltaEntity):

    @classmethod
    def new(cls, parent, dbus_connection):
        dbus_object = cls(parent)
        dbus_object.construct(dbus_connection)

    def _on_method_call(self, *args):
        _, _, _, _, method, param, invocation = args
        user_data = method, param, invocation
        self._transmitter.transmit(user_data)

    def _delta_call_emit_signal(self, user_data):
        signal, param = user_data
        self._dbus_connection.emit_signal(
            None,
            DBusSpec.OBJECT_PATH,
            DBusSpec.INTERFACE_NAME,
            signal,
            param
            )

    def _delta_call_register_dbus_method_object(self, object_):
        self._transmitter.register_listener(object_)

    def construct(self, dbus_connection):
        self._dbus_connection = dbus_connection
        self._dbus_connection.register_object(
            DBusSpec.OBJECT_PATH,
            DBusSpec.CONTROL_INTERFACE,
            self._on_method_call,
            )

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        EchoMethods(self)
        EchoWatchers(self)

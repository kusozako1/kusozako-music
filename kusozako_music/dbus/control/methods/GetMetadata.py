# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_music.const import ApplicationSignals
from .Method import AlfaMethod


class DeltaGetMetadata(AlfaMethod):

    __method_name__ = "GetMetadata"

    def _get_return_value(self, title, artist, album):
        return GLib.Variant.new_tuple(
            GLib.Variant.new_string(title),
            GLib.Variant.new_string(artist),
            GLib.Variant.new_string(album)
            )

    def _invoke(self, param, invocation):
        now_playing = self._enquiry("delta > now playing")
        file_info = now_playing.get_current_file_info()
        if file_info is None:
            return_value = self._get_return_value("", "", "")
        else:
            return_value = self._get_return_value(
                file_info.get_attribute_string("kusozako1::title"),
                file_info.get_attribute_string("kusozako1::artist"),
                file_info.get_attribute_string("kusozako1::album")
                )
        invocation.return_value(return_value)

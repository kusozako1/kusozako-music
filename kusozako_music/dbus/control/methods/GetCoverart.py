# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_music.const import ApplicationSignals
from kusozako1.audio_cover_art.AudioCoverArt import FoxtrotAudioCoverArt
from .Method import AlfaMethod


class DeltaGetCoverart(AlfaMethod):

    __method_name__ = "GetCoverart"

    def _get_return_value(self, value):
        param = GLib.Variant.new_string(value)
        return GLib.Variant.new_tuple(param)

    def _timeout(self, uri, invocation):
        pixbuf = self._loader.get_for_uri(uri)
        pixbuf.savev(self._target_path, "png", [], [])
        return_value = self._get_return_value(self._target_path)
        invocation.return_value(return_value)

    def _invoke(self, param, invocation):
        now_playing = self._enquiry("delta > now playing")
        file_info = now_playing.get_current_file_info()
        if file_info is None:
            return_value = self._get_return_value("")
            invocation.return_value(return_value)
        else:
            gfile = file_info.get_attribute_object("standard::file")
            uri = gfile.get_uri()
            GLib.timeout_add(1, self._timeout, uri, invocation)

    def _on_initialize(self):
        gfile, _ = Gio.File.new_tmp("XXXXXXXX.png")
        self._target_path = gfile.get_path()
        self._loader = FoxtrotAudioCoverArt.new_for_maximum_size(300)

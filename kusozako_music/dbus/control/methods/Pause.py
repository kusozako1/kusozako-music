# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import ApplicationSignals
from .Method import AlfaMethod


class DeltaPause(AlfaMethod):

    __method_name__ = "Pause"

    def _invoke(self, param, invocation):
        user_data = ApplicationSignals.PLAYBACK_PAUSE, None
        self._raise("delta > application signal", user_data)
        invocation.return_value(None)

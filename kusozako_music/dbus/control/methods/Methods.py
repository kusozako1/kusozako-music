# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Play import DeltaPlay
from .Pause import DeltaPause
from .Previous import DeltaPrevious
from .Next import DeltaNext
from .GetMetadata import DeltaGetMetadata
from .GetCoverart import DeltaGetCoverart


class EchoMethods:

    def __init__(self, parent):
        DeltaPlay(parent)
        DeltaPause(parent)
        DeltaNext(parent)
        DeltaPrevious(parent)
        DeltaGetMetadata(parent)
        DeltaGetCoverart(parent)

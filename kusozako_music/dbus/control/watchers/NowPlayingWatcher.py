# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Metadata import DeltaMetadata
from .Coverart import DeltaCoverart


class EchoNowPlayingWatcher:

    def __init__(self, parent):
        DeltaMetadata(parent)
        DeltaCoverart(parent)

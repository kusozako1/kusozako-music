# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.audio_cover_art.AudioCoverArt import FoxtrotAudioCoverArt


class DeltaCoverart(DeltaEntity):

    def _on_notify(self, now_playing, file_info, loader):
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        pixbuf = loader.get_for_uri(uri)
        if pixbuf is None:
            return
        pixbuf.savev(self._target_path, "png", [], [])
        signal_param = GLib.Variant.new_string(self._target_path)
        user_data = "CoverartChanged", GLib.Variant.new_tuple(signal_param)
        self._raise("delta > emit signal", user_data)

    def _ensure_directory(self):
        gfile, _ = Gio.File.new_tmp("XXXXXXXX.png")
        self._target_path = gfile.get_path()

    def __init__(self, parent):
        self._parent = parent
        self._ensure_directory()
        loader = FoxtrotAudioCoverArt.new_for_maximum_size(300)
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify, loader)

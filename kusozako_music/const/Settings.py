# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import PlaybackMode
from kusozako_music.const import ViewerTypes


PLAYBACK_MODE = "playback", "mode"
PLAYBACK_MODE_DEFAULT = "playback", "mode", PlaybackMode.REPEAT_ALL
PLAYBACK_VOLUME = "playback", "software-volume"
PLAYBACK_VOLUME_DEFAULT = "playback", "software-volume", 0.25
VIEW_PAGE = "viewer", "type"
VIEW_PAGE_DEFAULT = "viewer", "type", ViewerTypes.GRID_VIEW

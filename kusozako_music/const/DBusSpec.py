# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

WELL_KNOWN_NAME_PLAYER = "com.gitlab.kusozako1.Music.Control"
OBJECT_PATH = "/com/gitlab/kusozako1/Music/Control"
INTERFACE_NAME = "com.gitlab.kusozako1.Music.Control"
INTROSPECTION_XML = """
<!DOCTYPE node PUBLIC
'-//freedesktop//DTD D-BUS Object Introspection 1.0//EN'
'http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd'>
<node>
    <interface name='com.gitlab.kusozako1.Music.Control'>
        <method name='Play'/>
        <method name='Pause'/>
        <method name='Previous'/>
        <method name='Next'/>
        <method name='GetMetadata'>
            <arg direction='out' name='title' type='s'/>
            <arg direction='out' name='artist' type='s'/>
            <arg direction='out' name='album' type='s'/>
        </method><method name='GetCoverart'>
            <arg direction='out' name='coverart-path' type='s'/>
        </method>
        <signal name='MetadataChanged'>
            <arg name='title' type='s'/>
            <arg name='artist' type='s'/>
            <arg name='album' type='s'/>
        </signal>
        <signal name='CoverartChanged'>
            <arg name='coverart' type='s'/>
        </signal>
    </interface>
</node>
"""
NODE_INFO = Gio.DBusNodeInfo.new_for_xml(INTROSPECTION_XML)
CONTROL_INTERFACE = NODE_INFO.interfaces[0]

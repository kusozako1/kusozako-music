# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GREETER = "greeter"
HOME = "home"
GRID_VIEW = "grid-view"
LIST_VIEW = "list-view"

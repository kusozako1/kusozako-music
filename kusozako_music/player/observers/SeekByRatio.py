# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music.const import ApplicationSignals
from kusozako_music.ApplicationObserver import AlfaApplicationObserver

SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class DeltaSeekByRatio(AlfaApplicationObserver):

    SIGNAL = ApplicationSignals.PLAYBACK_SEEK_BY_RATIO

    def _on_transmission_received(self, ratio):
        playbin = self._enquiry("delta > playbin")
        _, duration = playbin.query_duration(Gst.Format.TIME)
        if duration > 0:
            playbin.seek_simple(Gst.Format.TIME, SEEK_FLAGS, duration*ratio)

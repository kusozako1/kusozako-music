# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music.const import ApplicationSignals
from kusozako_music.ApplicationObserver import AlfaApplicationObserver


class DeltaPlayPlayback(AlfaApplicationObserver):

    SIGNAL = ApplicationSignals.PLAY_PLAYBACK

    def _on_transmission_received(self, param=None):
        now_playing = self._enquiry("delta > now playing")
        if now_playing.get_current_file_info() is None:
            return
        playbin = self._enquiry("delta > playbin")
        playbin.set_state(Gst.State.PLAYING)

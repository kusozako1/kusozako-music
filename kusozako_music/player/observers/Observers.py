# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .NowPlayingChanged import DeltaNowPlayingChanged
from .TogglePlayback import DeltaTogglePlayback
from .SeekByRatio import DeltaSeekByRatio
from .Rewind import DeltaRewind
from .PlayPlayback import DeltaPlayPlayback
from .PlaybackPause import DeltaPlaybackPause
from .AboutToClose import DeltaAboutToClose


class EchoObservers:

    def __init__(self, parent):
        DeltaNowPlayingChanged(parent)
        DeltaTogglePlayback(parent)
        DeltaSeekByRatio(parent)
        DeltaRewind(parent)
        DeltaPlayPlayback(parent)
        DeltaPlaybackPause(parent)
        DeltaAboutToClose(parent)

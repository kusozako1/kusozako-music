# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako1.Entity import DeltaEntity


class DeltaNowPlayingChanged(DeltaEntity):

    def _on_notify(self, now_playing, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        playbin = self._enquiry("delta > playbin")
        _, state_hook, _ = playbin.get_state(Gst.CLOCK_TIME_NONE)
        playbin.set_state(Gst.State.NULL)
        playbin.set_property("uri", uri)
        playbin.set_state(Gst.State.PLAYING)

    def __init__(self, parent):
        self._parent = parent
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako_music.const import ApplicationSignals
from kusozako_music.ApplicationObserver import AlfaApplicationObserver

SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class DeltaRewind(AlfaApplicationObserver):

    SIGNAL = ApplicationSignals.PLAYBACK_REWIND

    def _go_backward(self):
        user_data = ApplicationSignals.QUEUE_PREVIOUS, None
        self._raise("delta > application signal", user_data)

    def _try_rewind(self, playbin):
        _, position = playbin.query_position(Gst.Format.TIME)
        _, duration = playbin.query_duration(Gst.Format.TIME)
        if position/Gst.SECOND >= 5:
            playbin.seek_simple(Gst.Format.TIME, SEEK_FLAGS, 0)
        else:
            self._go_backward()

    def _on_transmission_received(self, param=None):
        playbin = self._enquiry("delta > playbin")
        _, state, _ = playbin.get_state(Gst.CLOCK_TIME_NONE)
        if state == Gst.State.PLAYING:
            self._try_rewind(playbin)
        else:
            self._go_backward()

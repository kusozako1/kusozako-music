# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaSoftwareVolume(DeltaEntity):

    def _set_volume(self, ratio):
        playbin = self._enquiry("delta > playbin")
        playbin.set_property("volume", ratio)

    def receive_transmission(self, user_data):
        group, key, ratio = user_data
        if group != "playback" or key != "software-volume":
            return
        self._set_volume(ratio)

    def __init__(self, parent):
        self._parent = parent
        query = "playback", "software-volume", 0.25
        ratio = self._enquiry("delta > settings", query)
        self._set_volume(ratio)
        self._raise("delta > register settings object", self)

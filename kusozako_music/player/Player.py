# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from kusozako1.Entity import DeltaEntity
from kusozako_music.Progress import FoxtrotProgress
from .Bus import DeltaBus
from .SoftwareVolume import DeltaSoftwareVolume
from .observers.Observers import EchoObservers


class DeltaPlayer(DeltaEntity):

    def _delta_info_playbin(self):
        return self._playbin

    def __init__(self, parent):
        self._parent = parent
        Gst.init()
        self._playbin = Gst.ElementFactory.make("playbin")
        self._playbin.set_property("flags", Gst.StreamType.AUDIO)
        self._playbin.no_more_pads()
        self._playbin.set_state(Gst.State.NULL)
        self._playbin.set_state(Gst.State.PAUSED)
        DeltaSoftwareVolume(self)
        FoxtrotProgress.init_for_playbin(self._playbin)
        DeltaBus(self)
        EchoObservers(self)

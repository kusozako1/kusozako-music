# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_music.const import ExtraOverlayPages
from kusozako_music.const import EditTagSignals


class DeltaSignalReceiver(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_OVERLAY:
            return
        page_name, file_info = signal_param
        if page_name == ExtraOverlayPages.EDIT_TAG:
            user_data = EditTagSignals.FILE_INFO_CHANGED, file_info
            self._raise("delta > dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)

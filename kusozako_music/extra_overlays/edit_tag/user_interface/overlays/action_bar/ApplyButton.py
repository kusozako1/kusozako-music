# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = EditTagSignals.APPLY_CHANGES, None
        self._raise("delta > dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, has_change = user_data
        if signal == EditTagSignals.HAS_CHANGES_CHANGED:
            self.set_sensitive(has_change)

    def __init__(self, parent):
        self._parent = parent
        container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.Button.__init__(
            self,
            label=_("Apply"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=8,
            )
        self.set_size_request(120, -1)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        container.append(self)
        spacer = Gtk.Box(hexpand=True)
        container.append(spacer)
        self._raise("delta > add to container", container)
        self._raise("delta > register dialog object", self)

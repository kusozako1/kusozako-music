# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaTitleLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label=_("Edit Audio Tag"),
            hexpand=True,
            margin_bottom=12
            )
        self.add_css_class("kusozako-large-title")
        self._raise("delta > add to container", self)

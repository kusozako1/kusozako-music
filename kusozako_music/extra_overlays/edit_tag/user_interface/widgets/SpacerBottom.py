# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSpacerBottom(Gtk.Box, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, vexpand=True)
        self.set_size_request(-1, 48)
        self._raise("delta > add to container", self)

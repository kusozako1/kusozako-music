# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_music.const import EditTagSignals

TEMPLATE = _("Source File : {}")


class DeltaDescription(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != EditTagSignals.FILE_INFO_CHANGED:
            return
        gfile = file_info.get_attribute_object("standard::file")
        shorten_path = HomeDirectory.shorten(gfile.get_path())
        label = TEMPLATE.format(shorten_path)
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            margin_start=16,
            margin_end=16,
            margin_bottom=24,
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            )
        self._raise("delta > add to container", self)
        self._raise("delta > register dialog object", self)

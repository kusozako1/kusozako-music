# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals


class AlfaRow(Gtk.Box, DeltaEntity):

    TITLE = "define-title-here"
    KEY = "define-key-here"

    def _on_changed(self, entry):
        signal_param = self.KEY, entry.get_text()
        user_data = EditTagSignals.METADATA_CHANGED, signal_param
        self._raise("delta > dialog signal", user_data)

    def _on_activate(self, entry):
        user_data = EditTagSignals.APPLY_CHANGES, None
        self._raise("delta > dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, metadata = user_data
        if signal != EditTagSignals.METADATA_INITIALIZED:
            return
        self._entry.disconnect(self._id)
        title = metadata[self.KEY]
        self._entry.set_text(title)
        self._entry.connect("changed", self._on_changed)
        self._entry.connect("activate", self._on_activate)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            )
        self.set_size_request(-1, 48)
        header_label = Gtk.Label(label=self.TITLE, xalign=1)
        header_label.set_size_request(80, -1)
        self.append(header_label)
        self._entry = Gtk.Entry(text="test", hexpand=True)
        self._id = self._entry.connect("changed", self._on_changed)
        self.append(self._entry)
        self._raise("delta > add to container", self)
        self._raise("delta > register dialog object", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals


class AlfaRow(Adw.EntryRow, DeltaEntity):

    TITLE = "define-title-here"
    KEY = "define-key-here"

    def _on_changed(self, entry):
        signal_param = self.KEY, entry.get_text()
        user_data = EditTagSignals.METADATA_CHANGED, signal_param
        self._raise("delta > dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, metadata = user_data
        if signal != EditTagSignals.METADATA_INITIALIZED:
            return
        self.disconnect(self._id)
        value = metadata[self.KEY]
        self.set_text(value)
        self.connect("changed", self._on_changed)

    def __init__(self, parent):
        self._parent = parent
        Adw.EntryRow.__init__(self, title=self.TITLE)
        self._id = self.connect("changed", self._on_changed)
        self.connect("activate", self._on_changed)
        self._raise("delta > add to container", self)
        self._raise("delta > register dialog object", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Row import AlfaRow


class DeltaAlbum(AlfaRow):

    TITLE = _("Album : ")
    KEY = "album"

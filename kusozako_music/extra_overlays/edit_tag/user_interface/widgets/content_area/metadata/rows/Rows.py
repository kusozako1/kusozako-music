# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Title import DeltaTitle
from .Artist import DeltaArtist
from .Album import DeltaAlbum


class EchoRows:

    def __init__(self, parent):
        DeltaTitle(parent)
        DeltaArtist(parent)
        DeltaAlbum(parent)

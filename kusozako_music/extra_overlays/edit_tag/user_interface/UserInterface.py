# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_music.const import ExtraOverlayPages
from .overlays.Overlays import EchoOverlays
from .widgets.Widgets import DeltaWidgets


class DeltaUserInterface(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _delta_info_overlay(self):
        return self

    def _on_get_child_position(self, overlay, widget, rectangle):
        return widget.compute_position(overlay, rectangle)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self)
        self.add_css_class("osd")
        self.connect("get-child-position", self._on_get_child_position)
        EchoOverlays(self)
        DeltaWidgets(self)
        signal_param = self, ExtraOverlayPages.EDIT_TAG
        user_data = MainWindowSignals.ADD_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)

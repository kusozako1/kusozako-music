# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .buffer.Buffer import DeltaBuffer
from .original.Original import DeltaOriginal
from .apply.Apply import DeltaApply


class DeltaModel(DeltaEntity):

    def _delta_info_buffer(self):
        return self._buffer.get_buffer()

    def _delta_info_has_changes(self):
        return self._original.has_changes()

    def __init__(self, parent):
        self._parent = parent
        self._buffer = DeltaBuffer(self)
        self._original = DeltaOriginal(self)
        DeltaApply(self)

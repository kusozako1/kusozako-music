# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MetadataInitialized import DeltaMetadataInitialized
from .MetadataChanged import DeltaMetadataChanged


class EchoObservers:

    def __init__(self, parent):
        DeltaMetadataInitialized(parent)
        DeltaMetadataChanged(parent)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals


class DeltaMetadataInitialized(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, metadata = user_data
        if signal != EditTagSignals.METADATA_INITIALIZED:
            return
        self._raise("delta > metadata initialized", metadata)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register dialog object", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import EditTagSignals


class DeltaApplyChanges(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != EditTagSignals.APPLY_CHANGES:
            return
        if not self._enquiry("delta > has changes"):
            return
        self._raise("delta > apply changes")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register dialog object", self)

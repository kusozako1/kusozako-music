# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .edit_tag.EditTag import DeltaEditTag


class EchoExtraOverlays:

    def __init__(self, parent):
        DeltaEditTag(parent)

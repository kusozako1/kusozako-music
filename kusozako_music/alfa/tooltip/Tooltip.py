# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_music.util import ReadableDuration
from .observers.Observers import EchoObservers


class AlfaTooltip(DeltaEntity):

    __source_widget_query__ = "define source widget query here."

    def _set_picture(self, box):
        if self._pixbuf is None:
            return
        texture = Gdk.Texture.new_for_pixbuf(self._pixbuf)
        picture = Gtk.Picture.new_for_paintable(texture)
        box.append(picture)

    def _set_label(self, box):
        text = self._file_info.get_attribute_string("kusozako1::title")
        text += "\n"+self._file_info.get_attribute_string("kusozako1::artist")
        text += "\n"+self._file_info.get_attribute_string("kusozako1::album")
        seconds = self._file_info.get_attribute_uint32("kusozako1::duration")
        text += "\n"+ReadableDuration.from_seconds(seconds)
        label = Gtk.Label(label=text)
        box.append(label)

    def _on_query_tooltip(self, widget, x, y, mode, tooltip):
        if self._file_info is None:
            return False
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)
        box.set_size_request(-1, 128)
        self._set_picture(box)
        self._set_label(box)
        tooltip.set_custom(box)
        return True

    def _delta_call_pixbuf_changed(self, pixbuf):
        self._pixbuf = pixbuf

    def _delta_call_file_info_changed(self, file_info):
        self._file_info = file_info

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._file_info = None
        EchoObservers(self)
        overlay = self._enquiry(self.__source_widget_query__)
        overlay.connect("query-tooltip", self._on_query_tooltip)

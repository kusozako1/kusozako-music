# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileInfoChanged import DeltaFileInfoChanged
from .MetadataChanged import DeltaMetadataChanged
from .CoverartChanged import DeltaCoverartChanged


class EchoObservers:

    def __init__(self, parent):
        DeltaFileInfoChanged(parent)
        DeltaMetadataChanged(parent)
        DeltaCoverartChanged(parent)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .VerticalAdjustment import DeltaVerticalAdjustment


class AlfaListItemFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _get_list_item_widget(self, now_playing):
        raise NotImplementedError()

    def _on_setup(self, factory, list_item):
        now_playing = self._vertical_adjustment.connect(list_item)
        list_item_widget = self._get_list_item_widget(now_playing)
        list_item.set_child(list_item_widget)

    def _on_bind(self, factory, list_item):
        if list_item is None:
            return
        now_playing = self._enquiry("delta > now playing")
        item_index = list_item.get_position()
        selected = now_playing.is_selected(item_index)
        file_info = list_item.get_item()
        list_item_widget = list_item.get_child()
        list_item_widget.bind(selected, file_info)

    def __init__(self, parent):
        self._parent = parent
        self._vertical_adjustment = DeltaVerticalAdjustment(self)
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)

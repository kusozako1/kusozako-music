# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaApplicationObserver(DeltaEntity):

    SIGNAL = "define acceptable signal here."

    def _on_transmission_received(self, param):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != self.SIGNAL:
            return
        self._on_transmission_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)

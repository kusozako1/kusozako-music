# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaDifference(DeltaEntity):

    def _get_difference(self, keyword):
        if keyword.startswith(self._previous_keyword):
            return Gtk.FilterChange.MORE_STRICT
        elif self._previous_keyword.startswith(keyword):
            return Gtk.FilterChange.LESS_STRICT
        return Gtk.FilterChange.DIFFERENT

    def get_difference(self, keyword):
        if not keyword:
            difference = Gtk.FilterChange.LESS_STRICT
        else:
            difference = self._get_difference(keyword)
        self._previous_keyword = keyword
        return difference

    def __init__(self, parent):
        self._parent = parent
        self._previous_keyword = ""

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals
from .Difference import DeltaDifference

ATTRIBUTES = ("kusozako1::title", "kusozako1::artist", "kusozako1::album")


class DeltaKeyword(DeltaEntity, Gtk.CustomFilter):

    def _get_match_from_attributes(self, file_info, keyword):
        for attribute_name in ATTRIBUTES:
            attribute = file_info.get_attribute_string(attribute_name)
            if keyword in GLib.utf8_strup(attribute, -1):
                return True
        return False

    def _match_func(self, file_info, user_data=None):
        if not self._keyword:
            return True
        if file_info is None:
            return False
        return self._get_match_from_attributes(file_info, self._keyword)

    def _signal_received(self, keyword):
        self._keyword = GLib.utf8_strup(keyword, -1)
        difference = self._difference.get_difference(keyword)
        self.changed(difference)

    def receive_transmission(self, user_data):
        signal, keyword = user_data
        if signal != ApplicationSignals.SEARCH_KEYWORD_CHANGED:
            return
        self._signal_received(keyword)

    def __init__(self, parent):
        self._parent = parent
        self._difference = DeltaDifference(self)
        self._keyword = ""
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > register application object", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_music.const import ApplicationSignals
from kusozako_music.alfa.ApplicationObserver import AlfaApplicationObserver
from kusozako_music.const import PlaybackMode


class DeltaQueuePrevious(AlfaApplicationObserver):

    __signal__ = ApplicationSignals.QUEUE_PREVIOUS

    def _get_next_index(self, current_index, model):
        query = "playback", "mode"
        mode = self._enquiry("delta > settings", query)
        if mode == PlaybackMode.REPEAT_ONE:
            index = current_index
        elif mode == PlaybackMode.REPEAT_ALL:
            index = current_index-1
        else:
            index = GLib.random_int_range(0, model.props.n_items)
        next_index = index % len(model)
        return next_index

    def _signal_received(self, param=None):
        model = self._enquiry("delta > viewer model")
        if len(model) == 0:
            return
        current_index = self._enquiry("delta > current index")
        next_index = self._get_next_index(current_index, model)
        file_info = model[next_index]
        self._raise("delta > new file info", file_info)

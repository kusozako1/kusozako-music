# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .PartFile import DeltaPartFile


class EchoFilters:

    def __init__(self, parent):
        DeltaPartFile(parent)

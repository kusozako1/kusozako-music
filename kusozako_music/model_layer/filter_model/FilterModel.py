# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .filters.Filters import EchoFilters


class DeltaFilterModel(Gtk.FilterListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _delta_call_add_filter(self, filter_):
        self._every_filter.append(filter_)

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        self._every_filter = Gtk.EveryFilter.new()
        file_filter = Gtk.FileFilter(mime_types=["audio/mp4", "audio/mpeg"])
        self._every_filter.append(file_filter)
        Gtk.FilterListModel.__init__(
            self,
            filter=self._every_filter,
            )
        EchoFilters(self)

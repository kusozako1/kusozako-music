# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaDirectoryModel(Gtk.DirectoryList, DeltaEntity):

    def _on_notify_loading(self, model, param_spec):
        if self._init:
            return
        user_data = ApplicationSignals.MODEL_LAYER_READY, None
        self._raise("delta > application signal", user_data)
        self._init = True

    def _on_items_changed(self, model, position, removed, added):
        if added == 1:
            file_info = model[position]
            print("items-changed", file_info.get_name(), model.is_loading())

    def _idle(self):
        path = GLib.get_user_special_dir(3)
        gfile = Gio.File.new_for_path(path)
        self.set_file(gfile)
        self.connect("items-changed", self._on_items_changed)
        self.connect("notify::loading", self._on_notify_loading)
        return GLib.SOURCE_REMOVE

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != ApplicationSignals.WIDGET_LAYER_READY:
            return
        GLib.idle_add(self._idle)

    def __init__(self, parent):
        self._parent = parent
        self._init = False
        Gtk.DirectoryList.__init__(self, attributes="*")
        self._raise("delta > register application object", self)

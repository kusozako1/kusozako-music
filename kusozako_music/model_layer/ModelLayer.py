# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .DirectoryModel import DeltaDirectoryModel
from .filter_model.FilterModel import DeltaFilterModel
from .map_model.MapModel import DeltaMapModel
from .find_model.FindModel import DeltaFindModel
from .now_playing.NowPlaying import DeltaNowPlaying


class DeltaModelLayer(DeltaEntity):

    def _delta_info_viewer_model(self):
        return self._viewer_model

    def _delta_info_now_playing(self):
        return self._now_playing

    def __init__(self, parent):
        self._parent = parent
        directory_model = DeltaDirectoryModel(self)
        filter_model = DeltaFilterModel.new(self, directory_model)
        map_model = DeltaMapModel.new(self, filter_model)
        find_model = DeltaFindModel.new(self, map_model)
        self._viewer_model = Gtk.SingleSelection.new(find_model)
        self._now_playing = DeltaNowPlaying(self)
        self._raise("delta > loopback model layer ready", self)

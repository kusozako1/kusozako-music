# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals
from .SearchEntry import DeltaSearchEntry


class DeltaSearchBar(Gtk.Revealer, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def _toggle(self):
        child_revealed = not self.get_reveal_child()
        self.set_reveal_child(child_revealed)
        user_data = ApplicationSignals.SEARCH_BAR_TOGGLED, child_revealed
        self._raise("delta > application signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == ApplicationSignals.TOGGLE_SEARCH_BAR:
            self._toggle()
        if signal == ApplicationSignals.SEARCH_BAR_TOGGLED:
            self.set_reveal_child(param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_reveal_child(False)
        self._box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self._box.set_opacity(0.8)
        self.set_child(self._box)
        self._box.add_css_class("kusozako-primary-container")
        DeltaSearchEntry(self)
        self._raise("delta > add to overlay", self)
        self._raise("delta > register application object", self)

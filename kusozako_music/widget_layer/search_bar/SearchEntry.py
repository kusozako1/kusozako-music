# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _timeout(self):
        text = self.get_text()
        if text:
            return
        user_data = ApplicationSignals.SEARCH_BAR_TOGGLED, False
        self._raise("delta > application signal", user_data)
        return GLib.SOURCE_REMOVE

    def _on_search_changed(self, search_entry):
        text = search_entry.get_text()
        user_data = ApplicationSignals.SEARCH_KEYWORD_CHANGED, text
        self._raise("delta > application signal", user_data)
        if text == "":
            GLib.timeout_add_seconds(3, self._timeout)

    def _signal_received(self, child_revealed):
        if child_revealed:
            root = self.get_root()
            root.set_focus(self)
        else:
            self.set_text("")

    def receive_transmission(self, user_data):
        signal, child_revealed = user_data
        if signal != ApplicationSignals.SEARCH_BAR_TOGGLED:
            return
        self._signal_received(child_revealed)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            hexpand=True,
            search_delay=1000,
            )
        self.connect("search-changed", self._on_search_changed)
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .OrientationChanger import DeltaOrientationChanger
from .cover_art.CoverArt import DeltaCoverArt
from .metadata.Metadata import DeltaMetadata


class DeltaBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_info_box(self):
        return self

    def _delta_info_orientation(self):
        return self.props.orientation

    def _delta_call_orientation_changed(self, is_horizontal):
        if is_horizontal:
            self._metadata.props.xalign = 0
        else:
            self._metadata.props.xalign = 0.5

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, homogeneous=True, spacing=16)
        self.add_css_class("kusozako-content-area-medium")
        DeltaOrientationChanger(self)
        DeltaCoverArt(self)
        self._metadata = DeltaMetadata(self)
        self._raise("delta > add to container", self)

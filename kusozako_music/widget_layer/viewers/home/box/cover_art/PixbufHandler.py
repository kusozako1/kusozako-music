# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from kusozako1.audio_cover_art.AudioCoverArt import FoxtrotAudioCoverArt
from kusozako1.Entity import DeltaEntity


class DeltaPixbufHandler(DeltaEntity):

    def get_pixbuf(self, width, height):
        if self._pixbuf is None:
            return None
        pixbuf_width = self._pixbuf.get_width()
        pixbuf_height = self._pixbuf.get_height()
        horizontal_ratio = min(1, width/pixbuf_width)
        vertical_ratio = min(1, height/pixbuf_height)
        ratio = min(horizontal_ratio, vertical_ratio)
        return self._pixbuf.scale_simple(
            pixbuf_width*ratio,
            pixbuf_height*ratio,
            GdkPixbuf.InterpType.BILINEAR,
            )

    def _on_notify(self, now_playing, file_info, loader):
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        self._pixbuf = loader.get_for_uri(uri)
        self._raise("delta > request queue draw")

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        loader = FoxtrotAudioCoverArt.new_for_maximum_size(300)
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify, loader)

# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from .PixbufHandler import DeltaPixbufHandler


class DeltaCoverArt(Gtk.DrawingArea, DeltaEntity):

    def _get_x_position(self, pixbuf, width):
        orientation = self._enquiry("delta > orientation")
        if orientation == Gtk.Orientation.HORIZONTAL:
            return width - pixbuf.get_width()
        return (width-pixbuf.get_width())/2

    def _draw_func(self, drawing_area, cairo_context, width, height):
        scaled_pixbuf = self._pixbuf_handler.get_pixbuf(width, height)
        if scaled_pixbuf is None:
            return
        x = self._get_x_position(scaled_pixbuf, width)
        y = (height-scaled_pixbuf.get_height())/2
        Gdk.cairo_set_source_pixbuf(cairo_context, scaled_pixbuf, x, y)
        cairo_context.paint()

    def _delta_call_request_queue_draw(self):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf_handler = DeltaPixbufHandler(self)
        Gtk.DrawingArea.__init__(
            self,
            margin_top=16,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.set_size_request(-1, 240)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add to container", self)

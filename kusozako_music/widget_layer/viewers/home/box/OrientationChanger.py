# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaOrientationChanger(DeltaEntity):

    def _on_layout(self, surface, width, height, widget):
        if width >= height:
            widget.props.orientation = Gtk.Orientation.HORIZONTAL
            widget.props.homogeneous = True
            is_horizontal = True
        else:
            widget.props.orientation = Gtk.Orientation.VERTICAL
            widget.props.homogeneous = False
            is_horizontal = False
        self._raise("delta > orientation changed", is_horizontal)

    def _on_realize(self, widget):
        native = widget.get_native()
        surface = native.get_surface()
        surface.connect("layout", self._on_layout, widget)

    def __init__(self, parent):
        self._parent = parent
        box = self._enquiry("delta > box")
        box.connect("realize", self._on_realize)

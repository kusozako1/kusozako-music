# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Markup import FoxtrotMarkup


class DeltaMetadata(Gtk.Label, DeltaEntity):

    def _on_notify(self, now_playing, file_info):
        markup = self._markup.build_for_file_info(file_info)
        self.set_markup(markup)

    def _on_metadata_changed(self, now_playing, file_info, buffer):
        if file_info != now_playing.get_current_file_info():
            return
        markup = self._markup.build_for_buffer(buffer)
        self.set_markup(markup)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.Label.__init__(
            self,
            wrap=True,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            use_markup=True,
            vexpand=True,
            yalign=0.5,
            )
        self.add_css_class("kusozako-text-backlight")
        self._markup = FoxtrotMarkup()
        now_playing = self._enquiry("delta > now playing")
        now_playing.connect("now-playing-changed", self._on_notify)
        now_playing.connect("metadata-changed", self._on_metadata_changed)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)

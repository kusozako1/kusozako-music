# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height, rgba):
        sample = self._sample.copy()
        length = len(sample)
        x_step = width/(length)
        cairo_context.set_source_rgba(rgba.red, rgba.green, rgba.blue, 0.5)
        for index in range(0, length):
            bar_height = height*sample[index]
            x = index*x_step
            y = height-bar_height
            cairo_context.rectangle(x, y, x_step, bar_height)
        cairo_context.fill()

    def set_sample(self, sample):
        self._sample = sample

    def _timeout(self):
        # 20 frame per seconds.
        self.queue_draw()
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        self._sample = []
        query = "css", "primary_bg_color"
        settings = self._enquiry("delta > settings", query)
        self._rgba = Gdk.RGBA()
        self._rgba.parse(settings)
        Gtk.DrawingArea.__init__(self, hexpand=True, vexpand=True)
        self.set_draw_func(self._draw_func, self._rgba)
        GLib.timeout_add(50, self._timeout)
        self._raise("delta > add overlay", self)

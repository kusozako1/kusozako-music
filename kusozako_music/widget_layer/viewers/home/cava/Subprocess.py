# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

TEMPLATE = """
[general]
bars = {}
framerate = 20
[output]
method = raw
raw_target = {}
bit_format = 16bit
"""


class FoxtrotSubprocess:

    def _get_config_path(self, fifo_path, n_bars):
        gfile, _ = Gio.File.new_tmp("XXXXXX.conf")
        config = TEMPLATE.format(n_bars, fifo_path)
        gfile.replace_contents(
            config.encode("utf-8"),         # content
            None,                           # etag
            False,                          # make backup
            Gio.FileCreateFlags.PRIVATE,    # file create flag
            None,                           # cancellable
            )
        return gfile.get_path()

    def kill(self):
        self._subprocess.force_exit()

    def __init__(self, fifo_path, n_bars):
        config_path = self._get_config_path(fifo_path, n_bars)
        command = ["cava", "-p", config_path]
        self._subprocess = Gio.Subprocess.new(
            command,
            Gio.SubprocessFlags.STDOUT_SILENCE,     # read fifo not stdout.
            )
        self._subprocess.wait_async(None, None, None)

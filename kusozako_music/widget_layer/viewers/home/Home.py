# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ViewerTypes
from .box.Box import DeltaBox
from .cava.Cava import DeltaCava


class DeltaHome(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add_overlay(widget)

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        if widget.__class__.__name__ == "DeltaDrawingArea":
            rectangle.height = rectangle.height - 40
            return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self, hexpand=True, vexpand=True)
        DeltaBox(self)
        DeltaCava(self)
        self.connect("get-child-position", self._on_get_child_position)
        user_data = self, ViewerTypes.HOME
        self._raise("delta > add to stack", user_data)

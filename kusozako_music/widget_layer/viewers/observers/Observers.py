# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ViewerType import DeltaViewerType
from .Find import DeltaFind


class EchoObservers:

    def __init__(self, parent):
        DeltaViewerType(parent)
        DeltaFind(parent)

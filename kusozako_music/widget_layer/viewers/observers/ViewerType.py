# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaViewerType(DeltaEntity):

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "viewer" and key == "type":
            self._raise("delta > switch stack to", value)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register settings object", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaMap(DeltaEntity):

    def _idle(self, viewer):
        now_playing = self._enquiry("delta > now playing")
        index = now_playing.get_current_index()
        # negative value means has no curret index.
        if 0 > index:
            return
        viewer.scroll_to(index, Gtk.ListScrollFlags.SELECT)

    def _on_event(self, viewer):
        viewer.queue_draw()
        GLib.idle_add(self._idle, viewer)

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        viewer.connect("map", self._on_event)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.alfa.list_item_factory.ListItemFactory import (
    AlfaListItemFactory
    )
from .list_item_widget.ListItemWidget import TangoListItemWidget


class DeltaFactory(AlfaListItemFactory):

    def _get_list_item_widget(self, now_playing):
        return TangoListItemWidget.new(now_playing)

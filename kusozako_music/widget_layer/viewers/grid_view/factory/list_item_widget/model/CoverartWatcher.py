# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.thumbnailer.Thumbnailer import FoxtrotThumbnailer
from kusozako_music.const import ListItemSignals


class DeltaCoverartWatcher(DeltaEntity):

    def _loaded(self, pixbuf, state):
        user_data = ListItemSignals.COVERART_CHANGED, pixbuf
        self._raise("delta > list item signal", user_data)

    def bind(self, file_info):
        self._thumbnailer.load_from_file_info_async_2(file_info, self._loaded)

    def __init__(self, parent):
        self._parent = parent
        self._thumbnailer = FoxtrotThumbnailer.get_default()

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GObject
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel
from .overlay.Overlay import DeltaOverlay

CONTEXT_MENU_CALLED = (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, ())


class TangoListItemWidget(Gtk.AspectFrame, DeltaEntity):

    __gsignals__ = {
        "context-menu-called": CONTEXT_MENU_CALLED
        }

    @classmethod
    def new(cls, now_playing):
        instance = cls()
        instance.construct(now_playing)
        return instance

    def bind(self, selected, file_info):
        self._model.bind(selected, file_info)

    def _delta_call_request_context_menu_called(self):
        self.emit("context-menu-called")

    def _delta_call_register_list_item_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_list_item_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def construct(self, now_playing):
        self._model = DeltaModel.new(self, now_playing)
        DeltaOverlay(self)

    def __init__(self):
        self._parent = None
        self._transmitter = FoxtrotTransmitter()
        Gtk.AspectFrame.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaGestureClick(Gtk.GestureClick, DeltaEntity):

    def _on_released(self, controller, *args):
        self._raise("delta > request context menu called")

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self, button=3)
        self.connect("released", self._on_released)
        self._raise("delta > add controller", self)

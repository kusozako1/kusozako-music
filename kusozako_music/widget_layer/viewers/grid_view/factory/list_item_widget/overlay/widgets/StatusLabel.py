# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import PangoCairo
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals
from kusozako1.util.LayoutFactory import FoxtrotLayoutFactory

SELECTED = _("NOW PLAYING"), (255/256, 165/256, 0/256, 0.8)


class DeltaStatusLabel(Gtk.DrawingArea, DeltaEntity):

    def _draw(self, cairo_context, label, color):
        layout = self._layout_factory.build_(cairo_context, 128, 128/2)
        markup = GLib.markup_escape_text(label, -1)
        layout.set_markup(markup)
        _, layout_height = layout.get_pixel_size()
        cairo_context.set_source_rgba(*color)
        label_height = layout_height+4*2
        cairo_context.rectangle(0, 0, 128, label_height)
        cairo_context.fill()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(4, 4)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._selected:
            self._draw(cairo_context, *SELECTED)

    def receive_transmission(self, user_data):
        signal, selected = user_data
        if signal != ListItemSignals.SELECTION_CHANGED:
            return
        self._selected = selected
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._selected = False
        self._layout_factory = FoxtrotLayoutFactory.get_default()
        Gtk.DrawingArea.__init__(self)
        self.set_size_request(-1, 32)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add overlay", self)
        self._raise("delta > register list item object", self)

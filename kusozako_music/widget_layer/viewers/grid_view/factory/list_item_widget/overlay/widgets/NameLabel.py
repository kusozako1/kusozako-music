# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import PangoCairo
from kusozako1.Entity import DeltaEntity
from kusozako1.util.LayoutFactory import FoxtrotLayoutFactory
from kusozako_music.const import ListItemSignals

SHADE_COLOR_RGBA = 25/256, 25/256, 112/256, 0.6


class DeltaNameLabel(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._file_info is None:
            return
        layout = self._layout_factory.build_(cairo_context, 128, 128*0.5)
        basename = self._file_info.get_attribute_string("kusozako1::title")
        layout.set_markup(GLib.markup_escape_text(basename, -1))
        _, layout_height = layout.get_pixel_size()
        cairo_context.set_source_rgba(*SHADE_COLOR_RGBA)
        x = 0
        rectangle_height = layout_height+4*2
        y = height-rectangle_height
        cairo_context.rectangle(x, y, width, rectangle_height)
        cairo_context.fill()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(4, 128-layout_height-4)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal == ListItemSignals.METADATA_CHANGED:
            self._file_info = file_info
            self.queue_draw()
        if signal == ListItemSignals.FILE_INFO_CHANGED:
            self._file_info = file_info
            self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._file_info = None
        self._layout_factory = FoxtrotLayoutFactory.get_default()
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self._raise("delta > register list item object", self)
        self._raise("delta > add overlay", self)

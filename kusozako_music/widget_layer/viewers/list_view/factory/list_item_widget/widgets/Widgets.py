# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CheckButton import DeltaCheckButton
from .DrawingArea import DeltaDrawingArea
from .Info import DeltaInfo
from .Duration import DeltaDuration


class EchoWidgets:

    def __init__(self, parent):
        DeltaCheckButton(parent)
        DeltaDrawingArea(parent)
        DeltaInfo(parent)
        DeltaDuration(parent)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .Tooltip import DeltaTooltip
from .model.Model import DeltaModel
from .widgets.Widgets import EchoWidgets


class TangoListItemWidget(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, selection_model):
        instance = cls()
        instance.construct(selection_model)
        return instance

    def _on_released(self, gesture_click, *args):
        pass

    def bind(self, selected, file_info):
        self._model.bind(selected, file_info)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_register_list_item_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_list_item_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_box(self):
        return self

    def construct(self, now_playing):
        self._model = DeltaModel.new(self, now_playing)

    def __init__(self):
        self._parent = None
        self._transmitter = FoxtrotTransmitter()
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            hexpand=True,
            spacing=8,
            has_tooltip=True,
            )
        DeltaTooltip(self)
        EchoWidgets(self)
        gesture_click = Gtk.GestureClick(button=3)
        gesture_click.connect("released", self._on_released)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ListItemSignals
from .NowPlayingWatcher import DeltaNowPlayingWatcher
from .CoverartWatcher import DeltaCoverartWatcher


class DeltaModel(DeltaEntity):

    @classmethod
    def new(cls, parent, now_playing):
        instance = cls(parent)
        instance.construct(now_playing)
        return instance

    def bind(self, selected, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if not gfile.query_exists():
            return
        self._now_playing.bind(selected, file_info)
        self._coverart.bind(file_info)
        user_data = ListItemSignals.FILE_INFO_CHANGED, file_info
        self._raise("delta > list item signal", user_data)

    def construct(self, now_playing):
        self._now_playing = DeltaNowPlayingWatcher.new(self, now_playing)
        self._coverart = DeltaCoverartWatcher(self)

    def __init__(self, parent):
        self._parent = parent

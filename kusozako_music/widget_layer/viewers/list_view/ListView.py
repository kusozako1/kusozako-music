# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ViewerTypes
from .factory.Factory import DeltaFactory
from .watchers.Watchers import EchoWatchers


class DeltaListView(Gtk.ListView, DeltaEntity):

    def _idle(self, vadjustment):
        bottom_edge = vadjustment.props.value+vadjustment.props.page_size
        bottom_margin = vadjustment.props.upper - bottom_edge
        if bottom_margin >= 42:
            self.set_margin_bottom(0)
        else:
            self.set_margin_bottom(42-bottom_margin)

    def _on_value_changed(self, vadjustment):
        GLib.idle_add(self._idle, vadjustment)

    def _delta_info_viewer(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        vadjustment = scrolled_window.get_vadjustment()
        vadjustment.connect("value-changed", self._on_value_changed)
        clamp = Adw.ClampScrollable(maximum_size=640)
        Gtk.ListView.__init__(
            self,
            model=self._enquiry("delta > viewer model"),
            factory=DeltaFactory(self),
            show_separators=True,
            opacity=0.9,
            )
        self.add_css_class("card")
        EchoWatchers(self)
        clamp.set_child(self)
        scrolled_window.set_child(clamp)
        user_data = scrolled_window, ViewerTypes.LIST_VIEW
        self._raise("delta > add to stack", user_data)

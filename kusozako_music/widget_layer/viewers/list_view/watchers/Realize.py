# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaRealize(DeltaEntity):

    def _on_event(self, viewer):
        viewer.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        viewer.connect("realize", self._on_event)

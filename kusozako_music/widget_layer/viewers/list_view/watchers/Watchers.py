# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Realize import DeltaRealize
from .Map import DeltaMap
from .Activate import DeltaActivate


class EchoWatchers:

    def __init__(self, parent):
        DeltaRealize(parent)
        DeltaMap(parent)
        DeltaActivate(parent)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .menu_button.MenuButton import DeltaMenuButton
from .EditTagButton import DeltaEditTagButton
from .ViewSwitch import DeltaViewSwitch
from .BackwardButton import DeltaBackwardButton
from .ToggleButton import DeltaToggleButton
from .ForwardButton import DeltaForwardButton
from .progress_bar.ProgressBar import DeltaProgressBar
from .shuffle_button.ShuffleButton import DeltaShuffleButton
from .volume_button.VolumeButton import DeltaVolumeButton


class DeltaControlBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_opacity(0.8)
        self.add_css_class("kusozako-primary-container")
        DeltaMenuButton(self)
        DeltaEditTagButton(self)
        DeltaViewSwitch(self)
        DeltaBackwardButton(self)
        DeltaToggleButton(self)
        DeltaForwardButton(self)
        DeltaProgressBar(self)
        DeltaShuffleButton(self)
        DeltaVolumeButton(self)
        self._raise("delta > add to overlay", self)

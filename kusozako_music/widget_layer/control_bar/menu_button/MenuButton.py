# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage


class DeltaMenuButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        param = OverlayPage.PRIMARY_MENU, None
        user_data = MainWindowSignals.SHOW_EXTRA_OVERLAY, param
        self._raise("delta > main window signal", user_data)
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU, "music"
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            icon_name="view-more-symbolic",
            tooltip_text=_("Menu"),
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

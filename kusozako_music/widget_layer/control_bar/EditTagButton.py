# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_music.const import ExtraOverlayPages


class DeltaEditTagButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        now_playing = self._enquiry("delta > now playing")
        file_info = now_playing.get_current_file_info()
        if file_info is None:
            return
        signal_param = ExtraOverlayPages.EDIT_TAG, file_info
        user_data = MainWindowSignals.SHOW_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            icon_name="text-editor-symbolic",
            tooltip_text=_("Edit Tag"),
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

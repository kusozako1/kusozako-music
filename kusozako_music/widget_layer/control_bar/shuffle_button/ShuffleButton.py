# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Settings import DeltaSettings


class DeltaShuffleButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._settings.change_mode()

    def _delta_call_mode_changed(self, user_data):
        tooltip_text, icon = user_data
        self.props.tooltip_text = tooltip_text
        self.props.icon_name = icon

    def __init__(self, parent):
        self._parent = parent
        self._settings = DeltaSettings(self)
        Gtk.Button.__init__(
            self,
            has_frame=False,
            icon_name=self._settings.get_icon_name(),
            tooltip_text=self._settings.get_tooltip_text(),
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

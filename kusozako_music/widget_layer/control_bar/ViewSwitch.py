# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ViewerTypes as Types

DATA = {
        Types.HOME: ("view-grid-symbolic", Types.GRID_VIEW),
        Types.GRID_VIEW: ("view-list-bullet-symbolic", Types.LIST_VIEW),
        Types.LIST_VIEW: ("user-home-symbolic", Types.HOME)
    }


class DeltaViewSwitch(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = "viewer", "type", self._next
        self._raise("delta > settings", user_data)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "viewer" and key == "type":
            self._reset(value)

    def _reset(self, view_type=None):
        if view_type is None:
            user_data = "viewer", "type", Types.HOME
            view_type = self._enquiry("delta > settings", user_data)
        icon_name, self._next = DATA[view_type]
        self.set_icon_name(icon_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            margin_start=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self._reset()
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register settings object", self)

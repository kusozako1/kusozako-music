# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_music.const import ApplicationSignals


class DeltaClick(Gtk.GestureClick, DeltaEntity):

    def _on_pressed(self, gesture, n_press, x, y):
        widget = gesture.get_widget()
        ratio = x/widget.get_allocated_width()
        user_data = ApplicationSignals.PLAYBACK_SEEK_BY_RATIO, ratio
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self, button=1)
        self.connect("pressed", self._on_pressed)
        self._raise("delta > add controller", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity


class DeltaRGBA(DeltaEntity):

    def get_rgb(self):
        return self._rgba.red, self._rgba.green, self._rgba.blue

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "css" or key != "primary_fg_color":
            return
        self._rgba.parse(value)

    def __init__(self, parent):
        self._parent = parent
        self._rgba = Gdk.RGBA()
        query = "css", "primary_fg_color", "White"
        settings = self._enquiry("delta > settings", query)
        self._rgba.parse(settings)
        self._raise("delta > register settings object", self)

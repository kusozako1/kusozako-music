# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .RGBA import DeltaRGBA


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _try_draw_motion(self, cairo_context, width, height):
        ratio = self._enquiry("delta > motion ratio")
        if ratio == 0:
            return
        cairo_context.set_source_rgba(*self._rgb.get_rgb(), 0.25)
        cairo_context.rectangle(0, 0, width*ratio, height)
        cairo_context.fill()

    def _draw_func(self, drawing_area, cairo_context, width, height):
        position = self._enquiry("delta > position")
        if position == 0:
            return
        self._try_draw_motion(cairo_context, width, height)
        cairo_context.set_source_rgba(*self._rgb.get_rgb())
        cairo_context.rectangle(0, height-4, width*position, 4)
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
        self._rgb = DeltaRGBA(self)
        Gtk.DrawingArea.__init__(self, hexpand=True)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add to container", self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Settings import DeltaSettings
from .popover.Popover import DeltaPopover
from .Delay import DeltaDelay

TOOLTIP_TEMPLATE = "Audio Volume: {:.0%}"

ICON_NAMES = {
    0: "audio-volume-muted-symbolic",
    15: "audio-volume-low-symbolic",
    30: "audio-volume-medium-symbolic",
    100: "audio-volume-high-symbolic"
    }


class DeltaVolumeButton(Gtk.MenuButton, DeltaEntity):

    def _get_icon(self, ratio):
        for key, value in ICON_NAMES.items():
            if key >= ratio*100:
                return value
        return "audio-volume-overamplified-symbolic"

    def _delta_info_ratio(self):
        return self._settings.get_ratio()

    def _delta_call_change_ratio(self, ratio):
        self._settings.set_ratio(ratio)

    def _delta_call_popdown(self):
        self.props.popover.popdown()

    def _delta_call_ratio_changed(self, ratio):
        self.props.popover.queue_draw()
        self._reset(ratio)
        self._delay.start_idle()

    def _delta_call_start_idle(self):
        self._delay.start_idle()

    def _reset(self, ratio):
        self.props.icon_name = self._get_icon(ratio)
        self.props.tooltip_text = TOOLTIP_TEMPLATE.format(ratio)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MenuButton.__init__(
            self,
            has_frame=False,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            direction=Gtk.ArrowType.UP,
            popover=DeltaPopover(self),
            )
        self._delay = DeltaDelay(self)
        self._settings = DeltaSettings(self)
        self._reset(self._settings.get_ratio())
        self.set_size_request(32, 32)
        self.add_css_class("kusozako-primary-widget")
        self._raise("delta > add to container", self)

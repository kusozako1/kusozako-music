# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .RGBA import DeltaRGBA
from .controllers.Controllers import EchoControllers

LINE_WIDTH = 2
RADIUS = 8
MARGIN = 16


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        cairo_context.set_source_rgb(*self._rgb.get_rgb())
        cairo_context.set_line_width(2)
        cairo_context.move_to(width/2, 8)
        cairo_context.line_to(width/2, height-8)
        cairo_context.stroke()
        ratio = self._enquiry("delta > ratio")
        y = (100 - ratio*100)+MARGIN
        cairo_context.arc(width/2, y, RADIUS, 0, 2*GLib.PI)
        cairo_context.fill()

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def __init__(self, parent):
        self._parent = parent
        self._rgb = DeltaRGBA(self)
        Gtk.DrawingArea.__init__(self)
        self.set_size_request(32, 132)
        self.set_draw_func(self._draw_func)
        EchoControllers(self)
        self._raise("delta > add to container", self)

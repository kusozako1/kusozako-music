# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .drawing_area.DrawingArea import DeltaDrawingArea


class DeltaPopover(Gtk.Popover, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _on_map(self, args):
        self._raise("delta > start idle")

    def queue_draw(self):
        self._drawing_area.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Popover.__init__(self)
        self.connect("map", self._on_map)
        self._drawing_area = DeltaDrawingArea(self)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_music.const.Settings import PLAYBACK_VOLUME
from kusozako_music.const.Settings import PLAYBACK_VOLUME_DEFAULT


class DeltaSettings(DeltaEntity):

    def _timeout(self, ratio, index):
        if self._index != index:
            return
        user_data = PLAYBACK_VOLUME + (ratio,)
        self._raise("delta > settings", user_data)

    def get_ratio(self):
        return self._ratio

    def set_ratio(self, ratio):
        self._ratio = ratio
        index = self._index
        GLib.timeout_add(250, self._timeout, ratio, index)
        self._raise("delta > ratio changed", ratio)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        query = PLAYBACK_VOLUME_DEFAULT
        self._ratio = self._enquiry("delta > settings", query)

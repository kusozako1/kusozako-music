# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Label import DeltaLabel
from .RepeatOne import DeltaRepeatOne
from .RepeatAll import DeltaRepeatAll
from .Shuffle import DeltaShuffle


class EchoRepeatMode:

    def __init__(self, parent):
        DeltaLabel.new_for_label(parent, _("Repeat Mode"))
        DeltaRepeatOne(parent)
        DeltaRepeatAll(parent)
        DeltaShuffle(parent)

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_music.const import PlaybackMode
from .RepeatModeButton import AlfaRepeatModeButton


class DeltaShuffle(AlfaRepeatModeButton):

    LABEL = _("Shuffle")
    MATCH_VALUE = PlaybackMode.SHUFFLE

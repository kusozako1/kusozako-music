# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton


class AlfaViewTypeButton(AlfaSettingsButton):

    GROUP = "viewer"
    KEY = "type"
